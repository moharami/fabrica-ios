import React, {Component} from 'react';
import {View, Text, Container, Button, Content, Item, Input, Form, Icon, Footer, FooterTab, Spinner} from 'native-base';
import {FlatList, Image, TouchableOpacity, AsyncStorage, BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { list,filter } from './../assets/styles';
import { pexios, url} from '../component/helper'
import Tender from "../component/tender";
import {CustomPicker} from "react-native-custom-picker";
import {shopL, single} from "../assets/styles";

export default class myAds extends Component{
    constructor(props){
        super(props)
        this.state={
            categories:[],
            lastPage:0,
            advertisements:[],
            page:1,
            adLoading:false,
            refreshing:false,
            special:false,
            selectedCat:'',
            q:'',
            price:null
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/core/is-special','get').then(res=>{
            this.setState({
                special:res.data.data
            },()=>{
                if(this.state.special){
                    pexios('/tender/category','get').then(res=>{
                        this.setState({
                            categories:res.data.data
                        })
                    })
                    this.getAdRequest();
                }else{
                    pexios('/financial/prices?title=specialUser','get').then(res=>{
                        this.setState({
                            price:res.data.data
                        })
                    })
                }
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={list.container}>
                <Content style={{flex:1}}  contentContainerStyle={{flex: 1}}>
                    {
                        this.state.special?
                            <View>
                                <View style={list.searchParent}>
                                    <Item regular style={list.searchItem}>
                                        <Input placeholder='جستجو در مناقصات' style={[list.searchInput,{paddingRight:10}]} onChangeText={(val)=>{this.setState({q:val})}} />
                                        <Icon name={'md-search'} style={{position:'absolute',left:0}} onPress={()=>{this.setState({page:1,advertisements:[],selectedCat:''},()=>{this.getAdRequest();})}}  />
                                    </Item>
                                </View>
                                <View style={list.TenderCatParent}>
                                    {
                                        this.state.categories.map((cat)=>{
                                            return <TouchableOpacity key={cat.id} style={list.TenderCat} onPress={()=>{this.setState({selectedCat:cat.id,advertisements:[],page:1,q:''},()=>{this.getAdRequest();})}}><Text style={list.TenderCatText}>{cat.title}</Text></TouchableOpacity>
                                        })
                                    }
                                </View>
                                <FlatList
                                    ListFooterComponent={this.renderFooter.bind(this)}
                                    data={this.state.advertisements}
                                    renderItem={this.renderItem}
                                    keyExtractor={(item) => item.id.toString()}
                                    onEndReached={this.handleLoadMore.bind(this)}
                                    onEndReachedThreshold={0.01}
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.handleRefresh.bind(this)}
                                />
                            </View>:
                            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                                <Text style={list.specialText}>این بخش فقط برای کاربران ویژه در دسترس است</Text>
                                <Text style={list.specialText}> قیمت : {this.state.price} تومان </Text>
                                <View style={{marginTop:10}}>
                                    <Button onPress={this.specialRequest.bind(this)}>
                                        <Text style={list.specialText}>ارسال درخواست</Text>
                                    </Button>
                                </View>
                            </View>
                    }

                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[list.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.myFabrika()}>
                            <Icon style={list.footerIcon} name="md-person" />
                            <Text style={list.footerText}>فابریکای من</Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>Actions.replace('list')}>
                            <Icon style={shopL.footerIcon} name="md-home" />
                            <Text style={shopL.footerText}>صفحه اصلی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    getAdRequest() {
        const { page } = this.state;
        if(!this.state.adLoading){
            this.setState({
                adLoading:true
            })
            pexios('/tender/lists?page='+page+'&id='+this.state.selectedCat+'&q='+this.state.q,'get').then(res=>{
                let adv = res.data.data.data;
                this.setState(prevState=>{
                    return {
                        advertisements: page===1?adv:[...prevState.advertisements, ...adv],
                        page: res.data.data.current_page + 1,
                        lastPage: res.data.data.last_page,
                        refreshing : false
                    }
                })
                this.setState({adLoading:false})
            }).catch(er=>{
                console.log(er.response)
            })
        }
    }
    renderItem({ item }) {
        return <Tender ad={item} />
    }
    handleLoadMore(){
        if(this.state.advertisements.length > 0 && this.state.page <= this.state.lastPage) {
            this.getAdRequest()
        }
    }
    handleRefresh(){
        this.setState({ page : 1 , refreshing : true } , () => {
            this.getAdRequest();
        })
    }
    renderFooter(){
        if(!this.state.adLoading) return null;

        return <View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
    }
    specialRequest(){
        pexios('/financial/create_invoice','post',{title:'specialUser'}).then(res=>{
            console.log(res.data)
        })
    }
}