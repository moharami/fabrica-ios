import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    Spinner,
    Textarea,
    CheckBox, Label, Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { createAd } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {Alert, BackHandler, Image, Switch, TextInput} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {signup} from "../assets/styles";

export default class EditAd extends Component{
    constructor(props){
        super(props)
        this.state={
            priority:[
                {
                    title:'عادی',
                    key:'normal',
                },
                {
                    title:'فوری',
                    key:'immediate',
                },
                {
                    title:'ویژه',
                    key:'special',
                },
                {
                    title:'ویژه فوری',
                    key:'immediate-special',
                },
            ],
            type:[
                {
                    title:'فروش',
                    key:'sale',
                },
                {
                    title:'اجاره',
                    key:'rent',
                },
                {
                    title:'خرید',
                    key:'buy',
                },
            ],
            lastImages:[],
            images:[],
            subCatPicker:null,
            subCatLoader:false,
            categoryItems:[],
            itemLoader:false,
            switch:[],
            expiration_time:null,
            selectedCity:null,
            selectedCat:null,
            disableBtn:false,
            description:this.props.ad.description,
            offer:this.props.ad.offer==0?false:true,
            selectedType:this.props.ad.type,
            selectedPriority:this.props.ad.priority,
            datas:JSON.parse(this.props.ad.data),
            title:this.props.ad.title,
            price:this.props.ad.price,
            requiredFields:[],
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.props.ad.category.items.map((sw)=>{if(sw.type=='switch'){
            this.setState(prevState=> {
                return {
                    switch: [...prevState.switch, {id: sw.id, value: sw.value==1?true:false}],
                }
            })
        }})
        this.props.ad.attachmentsImage.map((img)=>{
            let image = {uri : url+'/files?uid='+img.uid+'&width=400&height=300',uid:img.uid}
            this.setState(prevState=>{
                return {
                    lastImages: [...prevState.lastImages, image],
                }
            })
        })
        for(let i = 0;i<this.props.ad.category.limit-this.props.ad.attachmentsImage.length;i++){
            let img = {uri : url+'/images/add_image.png',changed:false}
            this.setState(prevState=>{
                return {
                    images: [...prevState.images, img],
                }
            })
        }
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={createAd.container}>
                    <View style={{flex:1}}>
                        <Item regular style={createAd.inputCode} floatingLabel>
                            <Label style={createAd.inputLabel}>عنوان</Label>
                            <Input style={createAd.inputCodeChild} onChangeText={(value)=>this.setState({title:value})} value={this.state.title}/>
                        </Item>
                        <Item regular style={createAd.inputCode} floatingLabel>
                            <Label style={createAd.inputLabel}>قیمت</Label>
                            <Input style={createAd.inputCodeChild} keyboardType="numeric" onChangeText={(value)=>this.setState({price:value})} value={this.state.price.toString()} />
                        </Item>
                        <View style={createAd.inputCode}>
                            <Label style={createAd.inputLabel}>توضیحات</Label>
                            <TextInput style={createAd.editedTextArea} multiline={true} numberOfLines={5} onChangeText={(value)=>this.setState({description:value})} value={this.state.description} />
                        </View>
                        <Item style={{flexDirection:'row-reverse',justifyContent:'flex-start',paddingTop:10,paddingBottom:10}}>
                            <Switch
                                value={this.state.offer}
                                onValueChange={() => this.setState({offer:!this.state.offer})}
                            />
                            <Text style={createAd.switchText}>پیشنهاد قیمت توسط مشتری</Text>
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                value={this.state.priority[this.state.priority.indexOf(this.state.priority.filter((itm)=>{if(itm.key == this.state.selectedPriority ){return true}})[0])]}
                                options={this.state.priority}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.setState({selectedPriority:value.key}):this.setState({selectedPriority:null})}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                value={this.state.type[this.state.type.indexOf(this.state.type.filter((itm)=>{if(itm.key == this.state.selectedType ){return true}})[0])]}
                                placeholder={'نوع'}
                                options={this.state.type}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.setState({selectedType:value.key}):this.setState({selectedType:null})}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>

                        <View style={createAd.imgParent}>
                            {
                                this.state.images.map((img,index)=>{
                                    return <Button onPress={this.changeImage.bind(this,index)} key={index} transparent style={{width:100,height:100,marginTop:10}}><Image source={img} style={{width:100,height:100}} /></Button>
                                })
                            }
                            {
                                this.state.lastImages.map((img,index)=>{
                                    return <View key={index} transparent style={{width:100,height:100,marginTop:10}}><Image source={img} style={{width:100,height:100}} /><Icon onPress={this.deleteImage.bind(this,img.uid,index)} name='md-close' style={{ position:'absolute',left:5,top:5, color : '#c0392b'}}/></View>
                                })
                            }
                        </View>
                        {
                            this.props.ad.category.items.map((item)=>{
                                if(item.type=='text'){
                                    return (
                                        <Item key={item.id} regular  style={this.state.requiredFields.includes(item.id)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode} floatingLabel>
                                            <Label style={createAd.inputLabel}>{item.title}</Label>
                                            <Input style={createAd.inputCodeChild} onChangeText={this.changeText.bind(this,item.id)} value={this.state.datas[item.id]} />
                                        </Item>
                                    )
                                }
                                if(item.type == 'select'){
                                    return(
                                        <Item key={item.title} picker style={this.state.requiredFields.includes(item.id)?{textAlign:'right',borderColor:'red',borderWidth:1,borderStyle:'solid'}:{textAlign:'right'}}>
                                            <CustomPicker
                                                placeholder={item.title}
                                                options={JSON.parse(item.data)}
                                                value={JSON.parse(item.data)[JSON.parse(item.data).indexOf(JSON.parse(item.data).filter((itm)=>{if(itm == this.state.datas[item.id] ){return true}})[0])]}
                                                getLabel={item => item}
                                                containerStyle={createAd.picker}
                                                onValueChange={value=>{if(value!=null){this.state.datas[item.id]= value;this.forceUpdate()}else{this.state.datas[item.id]='';this.forceUpdate()}}}
                                                fieldTemplateProps={{
                                                    containerStyle: {
                                                        borderBottomColor: '#fff',
                                                        textAlign:'right'
                                                    },
                                                    textStyle: {
                                                        color: '#454545',
                                                        fontSize:18,
                                                        textAlign:'right',
                                                        right:0,
                                                        position:'absolute',
                                                        fontFamily:'IRANYekanMobileFN'
                                                    },
                                                }}
                                                optionTemplateProps={{
                                                    containerStyle: {
                                                        backgroundColor: '#fff',
                                                        height: 60,
                                                        borderBottomWidth:0,
                                                    },
                                                    textStyle: {
                                                        color: '#343434',
                                                        right:15,
                                                        fontFamily:'IRANYekanMobileFN',
                                                        position:'absolute'
                                                    }}}
                                            />
                                        </Item>
                                    )
                                }
                                if(item.type == 'switch' && this.state.switch.length){
                                    let index = this.state.switch.indexOf(this.state.switch.filter((sw)=>{if(sw.id == item.id){return true}})[0])
                                    return(
                                        <Item key={item.id} style={{flexDirection:'row-reverse',justifyContent:'flex-start',paddingTop:10,paddingBottom:10}}>
                                            <Switch
                                                value={this.state.switch[index].value}
                                                onValueChange={() => {this.state.switch[index].value=!this.state.switch[index].value;this.state.datas[item.id]=this.state.switch[index].value;this.forceUpdate();}}
                                            />
                                            <Text style={createAd.switchText}>{item.title}</Text>
                                        </Item>
                                    )
                                }
                                if(item.type == 'text-box'){
                                    return(
                                        <View key={item.id} style={this.state.requiredFields.includes(item.id)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode}>
                                            <Label style={createAd.inputLabel}>توضیحات</Label>
                                            <TextInput style={createAd.editedTextArea} multiline={true} numberOfLines={5}  onChangeText={this.changeText.bind(this,item.id)} value={this.state.datas[item.id]} />
                                        </View>
                                    )
                                }
                            })
                        }

                        <Button style={createAd.Button} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={createAd.ButtonText}>ویرایش آگهی</Text>
                            }
                        </Button>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
    changeText(id,value){
        this.state.datas[id]=value
        this.forceUpdate()
    }
    deleteImage(uid,index){
        pexios('/advertisement/delete-attachment','post',{uid:uid}).then(res=>{
            let img = {uri : url+'/images/add_image.png',changed:false}
            this.state.lastImages.splice(index,1)
            this.setState(prevState=>{
                return {
                    images: [...prevState.images, img],
                }
            })
            this.forceUpdate()
        })
    }
    changeImage(index,img){
        ImagePicker.openPicker({
            width: 400,
            height: 300,
            cropping: true
        }).then(image => {
            this.state.images[index]={uri:image.path,changed:true}
            this.forceUpdate()
        });
    }
    async submitForm(){
        this.setState({disableBtn:true,requiredFields:[]})
        let fd = object2formData({
            id:this.props.ad.id,
            title:this.state.title,
            price:this.state.price,
            type:this.state.selectedType,
            description:this.state.description,
            offer:this.state.offer?1:0
        })
        for (let i in this.state.datas){
            fd.append(`datas[${i}]`,this.state.datas[i])
            if(this.state.datas[i] === '' || !this.state.datas[i]){
                console.log(this.state.datas[i])
                await this.props.ad.category.items.map((cat)=>{
                    if(cat.id == i && cat.required ==1){
                        this.setState(prevState=>{
                            return{
                                requiredFields:[...prevState.requiredFields, cat.id]
                            }
                        })
                    }
                })
            }
        }
        this.state.images.map((img)=>{
            if(img.changed){
                fd.append("image[]",{uri:img.uri,type:"image/jpg",name:"image[]"});
            }
        })
        if(!this.state.requiredFields.length){
            if(this.state.selectedPriority==='normal'){
                pexios('/advertisement/update','post',fd).then(res=>{
                    this.setState({disableBtn:false})
                    Actions.reset('myAds')
                }).catch(err=>{
                    this.setState({disableBtn:false})
                    Toast.show({
                        text: 'برای تغییر آگهی فیلد های اجباری را پر نمایید!',
                        textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                        duration: 3000,
                        type:'danger'
                    })
                })
            }else {
                pexios('/advertisement/update','post',fd).then(res=>{
                    pexios('/financial/create_invoice','post',{title:'advertisement',adv_id:this.props.ad.id,type:this.state.selectedPriority}).then(res=>{
                        this.setState({disableBtn:false})
                        Alert.alert(
                            'ارور',
                            'نیازمند درگاه پرداخت',
                        );
                    })
                }).catch(err=>{
                    this.setState({disableBtn:false})
                    Toast.show({
                        text: 'برای تغییر آگهی فیلد های اجباری را پر نمایید!',
                        textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                        duration: 3000,
                        type:'danger'
                    })
                })
            }

        }else{
            this.setState({disableBtn:false})
            Toast.show({
                text: 'برای تغییر آگهی فیلد های اجباری را پر نمایید!',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }

    }
}