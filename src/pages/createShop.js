import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    CheckBox,
    Body,
    Textarea,
    Spinner, Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { signup } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {BackHandler, Image, Switch} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {createAd} from "../assets/styles";

export default class createShop extends Component{
    constructor(props){
        super(props)
        this.state={
            image:{ uri : url+'/images/shopBg.png'},
            logo:{ uri : url+'/images/profile.png'},
            states:[],
            city:[],
            name:null,
            mobile:null,
            tell:null,
            selectedCity:null,
            imageChanged:false,
            businessNumber:null,
            address:null,
            type:[
                {title:'ملک شخصی',value:1},
                {title:'ملک اجاره ای',value:0},
            ],
            selectedType:null,
            logoChanged:false,
            disableBtn:false,
            requiredFields:[],
            checkBox:[],
            categories:[],
            selectedCat:[],
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/data-states','get').then(res=>{
            this.setState({
                states:res.data
            })
        })
        pexios('/advertisement/category/list','get').then(res=>{
            this.setState({
                categories:res.data.data,
            },()=>{
                this.state.categories.map((cat)=>{
                    cat.subs.map((sub)=>{
                        this.setState(prevState=> {
                            return {
                                checkBox: [...prevState.checkBox, {id:sub.id,value:false}],
                            }
                        })
                    })
                })
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={signup.container}>
                    <View style={{flex:1}}>
                        <View style={{backgroundColor:'white',padding:10}}>
                            <Text style={signup.shopDesc}>
                                اگر در کار خرید و فروش اجناس فابریک و استوک صنعتی هستید، فابریکا فرصتی فراهم میکند که با راه اندازی فروشگاه، بیشتر دیده شوید و با کسب اعتبار و تبلیغات، مشتریان بیشتری جذب کنید.
                            </Text>
                        </View>
                        <Text style={signup.imgTitle}>تصویر لوگو</Text>
                        <View style={{flexDirection:'row',justifyContent:'center',marginTop:10}} >
                            <Button onPress={this.changeLogo.bind(this)} transparent style={{width:64,height:64}}>
                                <Image source={this.state.logo} style={{width:64,height:64,borderRadius:50}} />
                            </Button>
                        </View>
                        <Item regular style={this.state.requiredFields.includes(-1)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='نام فروشگاه' onChangeText={(val)=>{this.setState({name:val})}} />
                        </Item>
                        <Item regular style={this.state.requiredFields.includes(-2)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='شماره موبایل' keyboardType="numeric" onChangeText={(val)=>{this.setState({mobile:val})}} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='شماره ثابت فروشگاه' keyboardType="numeric" onChangeText={(val)=>{this.setState({tell:val})}} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='شماره پروانه کسب' onChangeText={(val)=>{this.setState({businessNumber:val})}} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Textarea rowSpan={5} bordered placeholder='آدرس فروشگاه (برای ایجاد فروشگاه مجازی این قسمت را خالی بگذارید)' style={signup.textArea} onChangeText={(val)=>{this.setState({address:val})}} />
                        </Item>
                        <Item picker style={this.state.requiredFields.includes(-4)?[{textAlign:'right'},{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'نوع ملک'}
                                options={this.state.type}
                                getLabel={item => item.title}
                                containerStyle={signup.picker}
                                onValueChange={value=>value!=null?this.setState({selectedType:value.value}):this.setState({selectedType:null})}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker style={this.state.requiredFields.includes(-3)?[{textAlign:'right'},{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب استان'}
                                options={this.state.states}
                                getLabel={item => item.title}
                                containerStyle={signup.picker}
                                onValueChange={value=>value!=null?this.getCity(value.id):this.getCity(0)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب شهر'}
                                options={this.state.city}
                                getLabel={item => item.title}
                                onValueChange={value=>value!=null?this.selectCity(value.city_id):this.selectCity(value.city_id)}
                                containerStyle={signup.picker}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Text style={signup.expertTitle}>دسته بندی هایی که در آن تخصص دارید را انتخاب نمایید</Text>
                        <View style={signup.catParent}>
                            {this.state.categories.map((cat)=>{
                                return(
                                    <View key={cat.id} style={{marginTop:15,borderBottomWidth:1,borderBottomColor:'#eee',padding:5}}>
                                        <Text style={signup.catTitle}>{cat.title}</Text>
                                        <View style={signup.subCatParent}>
                                            {
                                                cat.subs.map((sub)=>{
                                                    let index = this.state.checkBox.indexOf(this.state.checkBox.filter((cb)=>{if(cb.id == sub.id){return true}})[0])
                                                    return (
                                                        <View key={sub.id} style={{width:'50%',paddingRight:5,paddingLeft:5}}>
                                                            <Item style={{borderBottomWidth:0}} onPress={()=>this.selectCat(index,sub.id)}>
                                                                <Body style={{textAlign:'right',alignItems:'flex-end'}}>
                                                                    <Text style={signup.checkBoxText}>{sub.title}</Text>
                                                                </Body>
                                                                {
                                                                    this.state.checkBox.length?
                                                                        <CheckBox checked={this.state.checkBox[index].value} onPress={()=>this.selectCat(index,sub.id)} /> :null
                                                                }

                                                            </Item>
                                                        </View>
                                                    )
                                                })
                                            }
                                        </View>
                                    </View>
                                )
                            })}
                        </View>
                        <Button style={signup.loginButton} full onPress={this.submitForm.bind(this)}  disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={signup.loginButtonText}>ایجاد فروشگاه</Text>
                            }
                        </Button>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
    changeLogo(){
        ImagePicker.openPicker({
            width: 64,
            height: 64,
            cropping: true
        }).then(image => {
            this.setState({
                logo:{uri:image.path},
                logoChanged:true
            })
        });
    }
    selectCity(city_id){
        this.setState({
            selectedCity:city_id
        })
    }
    selectCat(index,id){
        if(this.state.checkBox[index].value){
            let catIndex = this.state.selectedCat.indexOf(id)
            if(catIndex>=0)
                this.state.selectedCat.splice(catIndex, 1)
        }else{
            this.state.selectedCat.push(id)
        }
        this.state.checkBox[index].value=!this.state.checkBox[index].value;
        this.forceUpdate();
    }
    async submitForm(){
        this.setState({
            disableBtn:true,
            requiredFields:[]
        })
        let fd = object2formData({title:this.state.name,tell:this.state.tell,mobile:this.state.mobile,city_id:this.state.selectedCity,owner:this.state.selectedType,address:this.state.address,business_licence:this.state.businessNumber})
        if(this.state.logoChanged){
            fd.append("logo",{uri:this.state.logo.uri,type:"image/jpg",name:"logo"});
        }

        if(!this.state.name || this.state.name==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -1]
                }
            })
        }
        if(!this.state.mobile || this.state.mobile==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -2]
                }
            })
        }
        if(!this.state.selectedCity || this.state.selectedCity==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -3]
                }
            })
        }
        if(this.state.selectedType == null){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -4]
                }
            })
        }
        if(!this.state.selectedCat.length){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -5]
                }
            })
        }
        for (let i in this.state.selectedCat){
            fd.append(`category_id[${i}]`,this.state.selectedCat[i])
        }

        console.log(this.state.requiredFields)
        if(!this.state.requiredFields.length){
            pexios('/shop/create','post',fd,true).then(res=>{
                this.setState({disableBtn:false})
                Actions.replace('myFabrika')
            }).catch(err=>{
                this.setState({disableBtn:false})
                Toast.show({
                    text: 'برای ثبت فروشگاه فیلد های اجباری را پر نمایید!',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'danger'
                })
            })
        }else{
            this.setState({disableBtn:false})
            Toast.show({
                text: 'برای ثبت فروشگاه فیلد های اجباری را پر نمایید!',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }
    }
    getCity(id){
        pexios('/advertisement/data-cities/'+id,'get').then(res=>{
            this.setState({
                city:res.data
            })
        })
    }
}