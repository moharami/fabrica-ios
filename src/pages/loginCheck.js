import React, {Component} from 'react';
import {View, Text, Container, Button, Content, Item, Input, Form, Toast, Spinner} from 'native-base';
import {AsyncStorage, BackHandler, Image} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { login } from './../assets/styles';
import {pexios} from '../component/helper'

export default class LoginCheck extends Component{
    constructor(props){
        super(props)
        this.state={
            mobile:null,
            disableBtn:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        if(this.props.mobile){
            this.setState({
                mobile:this.props.mobile
            })
        }
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={login.container}>
                <Content>
                    <View style={login.logoParent}>
                        <Image source={require('../assets/images/logo.png')} style={login.logo} />
                    </View>
                    <Item regular style={login.input}>
                        <Button style={login.inputButton} onPress={this.submitMobile.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={login.inputButtonText}>ارسال</Text>
                            }
                        </Button>
                        <Input style={login.inputChild} placeholder='شماره موبایل' keyboardType="numeric"  onChangeText={this.changeMobile.bind(this)} value={this.state.mobile} />
                    </Item>
                    <Button transparent full style={{marginTop:100}} onPress={()=>Actions.replace('list')}>
                        <Text style={login.loginButtonText}>ورود به اپلیکیشن بدون عضویت</Text>
                    </Button>
                </Content>
            </Container>
        )
    }
    changeMobile(mobile){
        this.setState({
            mobile:mobile
        })
    }
    submitMobile(){
        this.setState({
            disableBtn:true,
        })
        if(this.state.mobile != null){
            pexios('/check','post',{mobile:this.state.mobile}).then(res=>{
                this.setState({
                    disableBtn:false,
                })
                if(!res.data.data){
                    Actions.signup({mobile:this.state.mobile,shop:this.props.shop,expert:this.props.expert})
                }else{
                    Actions.login({mobile:this.state.mobile,shop:this.props.shop,expert:this.props.expert})
                }
            }).catch(er=>{
                this.setState({disableBtn:false})
            })
        }else{
            this.setState({
                disableBtn:false,
            })
            Toast.show({
                text: 'شماره موبایل خود را به درستی وارد نکردید',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }
    }
}