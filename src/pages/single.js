import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab, Toast, Spinner, ListItem, Left, Right, Radio, Textarea, Badge
} from 'native-base';
import {AsyncStorage, BackHandler, Image, ImageBackground, ScrollView, TouchableOpacity, Share} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Swiper from 'react-native-swiper';
import { single,filter } from './../assets/styles';
import moment from 'moment-jalaali';
import Communications from 'react-native-communications';
import {pexios, url ,priceFormat} from "../component/helper";
// import Share from 'react-native-share';
import Lightbox from 'react-native-lightbox';
import Advertisement from "../component/advertisement";
import {createAd, list} from "../assets/styles";

export default class Single extends Component{
    constructor(props){
        super(props)
        this.state={
            lightBox:false,
            reportLightBox:false,
            likeIcon:this.props.ad.liked?'bookmark':'bookmark-o',
            expertLightBox:false,
            city:'',
            state:'',
            login:null,
            offerPrice:null,
            disableOffer:false,
            offered:false,
            related:[],
            selectedResult:null,
            moreReport:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/city?id='+this.props.ad.city_id,'get').then(res=>{
            this.setState({
                city: res.data.title,
                state: res.data.state.title
            })
        })
        AsyncStorage.getItem('token',(error , result) => {
            if(result){
                this.setState({
                    login:true
                })
            }
        })
        pexios('advertisement/related?adv_id='+this.props.ad.id,'get').then(res=>{
            this.setState({
                related:res.data.data
            })
        })
    }
    handleBackButton(){
        Actions.replace('list')
        return true;
    }
    async onShare  () {
    try {
    const result = await Share.share({message:
             this.props.ad.title+' \n http://fabrica.ir/single/'+this.props.ad.id
        ,})

            if (result.action === Share.sharedAction) {
            if (result.activityType) {
            // shared with activity type of result.activityType
        } else {
            // shared
        }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
        } catch (error) {
            alert(error.message);
        }
    };
    render(){
        const { ad,myAd } = this.props;
        return(
            <Container style={single.container}>
                {this.state.lightBox?
                    <View style={filter.lightBox}>
                        <View style={filter.lightBoxContent}>
                            <Button transparent style={{ position: 'absolute', top : 0 , left : 0}} onPress={()=>this.setState({lightBox:false})}>
                                <Icon name='md-close-circle' style={{ fontSize : 30 , color : '#34495e'}}/>
                            </Button>
                            <Text style={filter.description}>آیا از حذف آگهی مطمئن هستید؟</Text>
                            <View style={{flexDirection:'row',justifyContent:'center',marginTop:20,marginBottom:20}}>
                                <Button style={filter.Button} onPress={()=>this.deleteAd(ad.id)} ><Text style={filter.ButtonText}> بله </Text></Button>
                            </View>
                        </View>
                    </View>:null
                }
                {this.state.reportLightBox?
                    <View style={filter.lightBox}>
                        <View style={filter.lightBoxContent}>
                            <Button transparent onPress={()=>this.setState({reportLightBox:false})}>
                                <Icon name='md-close-circle' style={{ fontSize : 30 , color : '#34495e'}}/>
                            </Button>
                            <ListItem onPress={()=>this.setState({selectedResult:'صاحب آگهی در دسترس نیست',moreReport:false})}>
                                <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                    <Radio onPress={()=>this.setState({selectedResult:'صاحب آگهی در دسترس نیست',moreReport:false})} selected={this.state.selectedResult=='صاحب آگهی در دسترس نیست'?true:false} />
                                    <Text style={[filter.ButtonText,{marginRight:10}]}>صاحب آگهی در دسترس نیست</Text>
                                </View>
                            </ListItem>
                            <ListItem onPress={()=>this.setState({selectedResult:'محصول یا خدمت آگهی شده دیگر موجود نیست',moreReport:false})}>
                                <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                    <Radio onPress={()=>this.setState({selectedResult:'محصول یا خدمت آگهی شده دیگر موجود نیست',moreReport:false})} selected={this.state.selectedResult=='محصول یا خدمت آگهی شده دیگر موجود نیست'?true:false} />
                                    <Text style={[filter.ButtonText,{marginRight:10}]}>محصول یا خدمت آگهی شده دیگر موجود نیست</Text>
                                </View>
                            </ListItem>
                            <ListItem onPress={()=>this.setState({selectedResult:'گروه بندی نادرست است',moreReport:false})}>
                                <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                    <Radio onPress={()=>this.setState({selectedResult:'گروه بندی نادرست است',moreReport:false})} selected={this.state.selectedResult=='گروه بندی نادرست است'?true:false} />
                                    <Text style={[filter.ButtonText,{marginRight:10}]}>گروه بندی نادرست است</Text>
                                </View>
                            </ListItem>
                            <ListItem onPress={()=>this.setState({selectedResult:'اطلاعات تماس نادرست است',moreReport:false})}>
                                <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                    <Radio onPress={()=>this.setState({selectedResult:'اطلاعات تماس نادرست است',moreReport:false})} selected={this.state.selectedResult=='اطلاعات تماس نادرست است'?true:false} />
                                    <Text style={[filter.ButtonText,{marginRight:10}]}>اطلاعات تماس نادرست است</Text>
                                </View>
                            </ListItem>
                            <ListItem onPress={()=>this.setState({moreReport:true})}>
                                <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                    <Radio onPress={()=>this.setState({moreReport:true})} selected={this.state.moreReport} />
                                    <Text style={[filter.ButtonText,{marginRight:10}]}>موارد دیگر</Text>
                                </View>
                            </ListItem>
                            {
                                this.state.moreReport?
                                <ListItem>
                                    <Textarea rowSpan={5} placeholder='توضیحات' style={createAd.textArea}  onChangeText={(value)=>this.setState({selectedResult:value})} />
                                </ListItem>:null
                            }
                            <View style={{flexDirection:'row',justifyContent:'center',marginTop:20,marginBottom:20}}>
                                <Button style={filter.Button} onPress={this.report.bind(this)} ><Text style={filter.ButtonText}> ارسال </Text></Button>
                            </View>
                        </View>
                    </View>:null
                }
                <Content style={{flex:1}}>
                    <View>
                        <Swiper style={{height:300}}  showsButtons={false}>
                            {
                                ad.attachmentsImage.length?
                                    ad.attachmentsImage.map((img)=>{
                                        return (
                                                <View key={img.id}>
                                                    <Lightbox underlayColor="white">
                                                        <ImageBackground source={{uri :url+'/files?uid='+img.uid+'&width=400&height=300'}}  style={{width:'100%',height:300}}>
                                                            <View style={{position:'absolute',top:0,left:0,right:0,bottom:0,backgroundColor:'rgba(0,0,0,.2)'}}></View>
                                                        </ImageBackground>
                                                    </Lightbox>
                                                </View>
                                            )
                                    }):
                                    <View>
                                        <ImageBackground source={require('../assets/images/no-image.png')}  style={{width:'100%',height:300}}>
                                            <View style={{position:'absolute',top:0,left:0,right:0,bottom:0,backgroundColor:'rgba(0,0,0,.2)'}}></View>
                                        </ImageBackground>
                                    </View>
                            }
                        </Swiper>
                        <TouchableOpacity  onPress={()=>Actions.pop()} style={{position:'absolute',left:25,top:35,padding:5}}><Icon style={{color:'white'}} name={'ios-arrow-back'} /></TouchableOpacity>
                        {
                            this.state.login?
                                <TouchableOpacity onPress={()=>this.likeAd(ad.id)} style={{position:'absolute',right:60,top:35}}><Icon style={{color:'white'}} type="FontAwesome" name={this.state.likeIcon} /></TouchableOpacity>
                                :null
                        }
                        <TouchableOpacity style={{position:'absolute',right:25,top:35}}><Icon style={{color:'white'}} name="md-share" onPress={this.onShare.bind(this)} /></TouchableOpacity>
                        <View style={single.special}><Text style={[single.specialText,single.bold]}>{ad.priority=='immediate'?'فوری':ad.priority=='special'?'ویژه':ad.priority=='immediate-special'?'ویژه و فوری':'عادی'}</Text></View>
                    </View>
                    {
                        myAd?
                            <View>
                                <View style={single.buttonParentEdit}>
                                    <View style={{flex:.5,marginRight:5}}>
                                        <Button block style={single.deleteButton} onPress={()=>this.setState({lightBox:true})} >
                                            <Text style={single.chatButtonText}>حذف</Text>
                                        </Button>
                                    </View>
                                    <View style={{flex:.5,marginLeft:5}}>
                                        <Button block style={single.editButton} onPress={()=>Actions.editAd({ad:ad})}>
                                            <Text style={single.chatButtonText}>ویرایش</Text>
                                        </Button>
                                    </View>
                                </View>
                            </View>:null
                    }
                    <View style={single.whiteBox}>
                        <Text style={single.mainTitle}>{ad.title}</Text>
                        <View style={{flexDirection:'row-reverse',justifyContent:'space-between'}}>
                            <Text style={single.date}>{moment(ad.created_at).format('jYYYY/jM/jD')}</Text>
                            <Text style={single.price}>{priceFormat(ad.price)} تومان</Text>
                        </View>
                        {

                                this.state.login?
                                    ad.offer==1?
                                        <View>
                                            <View style={{flexDirection:'row-reverse',marginTop:20}}>
                                                <Input style={single.offerInput}  placeholder='مبلغ پیشنهادی' keyboardType="numeric" onChangeText={(val)=>this.setState({offerPrice:val})} />
                                                <View style={{flex:.5}}>
                                                    <Button style={single.offerBtn} full onPress={this.offer.bind(this,ad.id)} disabled={this.state.disableOffer}>
                                                        {
                                                            this.state.offered?
                                                                <Icon name={'checkmark'} style={{color:'white'}} />
                                                                :
                                                                this.state.disableOffer?
                                                                    <Spinner color='#fff' />:
                                                                    <Text style={single.offerBtnText} >پیشنهاد دهید</Text>
                                                        }

                                                    </Button>
                                                </View>
                                            </View>
                                            {
                                                myAd?
                                                ad.offers.map((offer)=>{
                                                    return(
                                                            <View key={offer.id} style={{flexDirection:'row-reverse',justifyContent:'space-between',marginTop:10,alignItems:'center',}}>
                                                                <Image style={single.offerPic} source={{uri:offer.user.attachments?url+'/files?uid='+offer.user.attachments.uid+'&width=50&height=50':url+'/images/profile.png'}} />
                                                                <Text style={single.offerUser}>{offer.user.mobile}</Text>
                                                                <Text style={single.offerUser}>{offer.user.fname} {offer.user.lname}</Text>
                                                                <Text style={single.offerUser}>{offer.offer} تومان </Text>
                                                            </View>
                                                        )

                                                }):null
                                            }
                                        </View>
                                        :null
                                    :null
                        }
                        <Text style={[single.title,single.bold]}>مشخصات فنی:</Text>
                        {
                            ad.category.items.filter((tec)=>{return tec.technical==1}).map((item)=>{
                                    return <View style={single.detailView} key={item.id}><Text style={[single.detail,{textAlign:'right'}]}>{item.title}:</Text><Text style={[single.detail,{textAlign:'left'}]}>{JSON.parse(ad.data)?JSON.parse(ad.data)[item.id]?item.type=='switch'?JSON.parse(ad.data)[item.id]==1?'بله':'خیر':JSON.parse(ad.data)[item.id]:'':''}</Text></View>
                                }
                            )
                        }

                        <Text style={single.title}>شرایط مالی:</Text>
                        {
                            ad.category.items.filter((tec)=>{return tec.technical==0}).map((item)=>{
                                    return <View style={single.detailView} key={item.id}><Text style={[single.detail,{textAlign:'right'}]}>{item.title}:</Text><Text style={[single.detail,{textAlign:'left'}]}>{JSON.parse(ad.data)?JSON.parse(ad.data)[item.id]?item.type=='switch'?JSON.parse(ad.data)[item.id]==1?'بله':'خیر':JSON.parse(ad.data)[item.id]:'':''}</Text></View>
                                }
                            )
                        }
                    </View>
                    <View style={[single.whiteBox,{marginTop:5}]}>
                        <Text style={[single.title,single.bold]}>توضیحات:</Text>
                        <Text style={single.detailDesc}>
                            {ad.description}
                        </Text>
                    </View>
                    {
                        ad.shop?
                            <View style={[single.whiteBox,{marginTop:5}]}>
                                <Text style={[single.title,single.bold]}>مشخصات فنی:</Text>
                                <View style={single.detailView}>
                                    <Text style={[single.detail,{textAlign:'right'}]}>آگهی دهنده:</Text>
                                    <Text style={[single.detail,{textAlign:'left'}]}>{ad.shop.title}</Text>
                                </View>
                                <View style={single.detailView}>
                                    <Text style={[single.detail,{textAlign:'right'}]}>اعتبار فروشگاه:</Text>
                                    {
                                            ad.shop.type=='silver'?
                                                <Badge style={{backgroundColor:'#a4b0be'}}><Text style={list.shopStarText}>اعتبار نقره ای</Text></Badge>
                                                : ad.shop.type=='gold'?
                                                    <Badge style={{backgroundColor:'#eccc68'}}><Text style={list.shopStarText}>اعتبار طلایی</Text></Badge>

                                                    :null
                                    }
                                </View>
                                <View style={single.detailView}>
                                    <Text style={[single.detail,{textAlign:'right'}]}>استان:</Text>
                                    <Text style={[single.detail,{textAlign:'left'}]}>{this.state.state}</Text>
                                </View>
                                <View style={single.detailView}>
                                    <Text style={[single.detail,{textAlign:'right'}]}>شهر:</Text>
                                    <Text style={[single.detail,{textAlign:'left'}]}>{this.state.city}</Text>
                                </View>
                                <View style={single.detailView}>
                                    <Text style={[single.detail,{textAlign:'right'}]}>تماس همراه:</Text>
                                    <Text style={[single.detail,{textAlign:'left'}]}>{ad.shop.mobile}</Text>
                                </View>
                                <View style={single.detailView}>
                                    <Text style={[single.detail,{textAlign:'right'}]}>تماس ثابت:</Text>
                                    <Text style={[single.detail,{textAlign:'left'}]}>{ad.shop.tell}</Text>
                                </View>
                            </View>:
                            ad.user?
                                <View style={[single.whiteBox,{marginTop:5}]}>
                                    <Text style={[single.title,single.bold]}>مشخصات آگهی دهنده:</Text>
                                    <View style={single.detailView}>
                                        <Text style={[single.detail,{textAlign:'right'}]}>آگهی دهنده:</Text>
                                        <Text style={[single.detail,{textAlign:'left'}]}>{ad.user.fname} {ad.user.lname}</Text>
                                    </View>
                                    <View style={single.detailView}>
                                        <Text style={[single.detail,{textAlign:'right'}]}>استان:</Text>
                                        <Text style={[single.detail,{textAlign:'left'}]}>{this.state.state}</Text>
                                    </View>
                                    <View style={single.detailView}>
                                        <Text style={[single.detail,{textAlign:'right'}]}>شهر:</Text>
                                        <Text style={[single.detail,{textAlign:'left'}]}>{this.state.city}</Text>
                                    </View>
                                    <View style={single.detailView}>
                                        <Text style={[single.detail,{textAlign:'right'}]}>تماس همراه:</Text>
                                        <Text style={[single.detail,{textAlign:'left'}]}>{ad.user.mobile}</Text>
                                    </View>
                                    <View style={single.detailView}>
                                        <Text style={[single.detail,{textAlign:'right'}]}>ایمیل:</Text>
                                        <Text style={[single.detail,{textAlign:'left'}]}>{ad.user.email}</Text>
                                    </View>
                                </View>
                                :null

                    }
                    <View style={[single.whiteBox,{marginTop:5}]}>
                        <View style={{flexDirection:'row-reverse',alignItems:'center'}}>
                            <Icon name='shield' type='FontAwesome' style={single.warningIcon}  />
                            <Text style={single.warningText}>
                                {
                                    'خرید خود را حضوری انجام دهید و پیش از آن \n مبلغی واریز نکنید. پیشنهاد می شود از فروشگاه هایی که اعتبار دارند خرید کنید و هنگام خرید از کارشناسان خبره استفاده نمایید.'
                                }

                            </Text>
                        </View>
                    </View>
                    <View style={single.buttonParent}>
                        <View style={{flex:.5,marginRight:5}}>
                            <Button block style={single.chatButton} onPress={()=>this.state.login?Actions.chat({id:ad.user.id}):Actions.loginCheck()}>
                                <Text style={single.chatButtonText}>چت با آگهی دهنده</Text>
                            </Button>
                        </View>
                        <View style={{flex:.5,marginLeft:5}}>
                            <Button block style={single.reqButton} onPress={()=>this.state.login?Actions.expertList({ad:ad}):Actions.loginCheck()}>
                                <Text style={single.chatButtonText}>درخواست کارشناس</Text>
                            </Button>
                        </View>
                    </View>
                    <View>
                        {
                            this.state.related.length?   <Text style={single.relatedTitle}>آگهی های مرتبط</Text>:null
                        }
                        {
                            this.state.related.map((rel)=>{
                                return <Advertisement key={rel.id} ad={rel} myAd={false}/>
                            })
                        }
                    </View>

                    <View style={single.buttonParent}>
                        <View style={{width:'100%'}}>
                            <Button block style={single.chatButton} onPress={()=>this.state.login?this.setState({reportLightBox:true}):Actions.loginCheck()}>
                                <Text style={single.chatButtonText}>گزارش خطا</Text>
                            </Button>
                        </View>
                    </View>
                    <View style={{paddingTop:5}}></View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={() => Communications.text(ad.user?ad.user.mobile:ad.shop.mobile)}>
                            <Icon style={single.footerIcon} name="ios-mail" />
                            <Text style={single.footerText}>ارسال پیامک</Text>
                        </Button>
                        <Button style={single.pageFooterButton} onPress={() => Communications.phonecall(ad.user?ad.user.mobile:ad.shop.mobile, true)}>
                            <Icon style={single.footerIcon} name="ios-call" />
                            <Text style={single.footerText}>تماس تلفنی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    deleteAd(id){
        pexios('/advertisement/delete','post',{id:id}).then(res=>{
            Actions.myAds()
        })
    }
    likeAd(id){
        pexios('/advertisement/like','post',{id:id}).then(res=>{
            if(this.state.likeIcon=='bookmark'){
                this.setState({
                    likeIcon:'bookmark-o'
                })
            }else{
                this.setState({
                    likeIcon:'bookmark'
                })
            }

        })
    }
    offer(id){
        if(this.state.offerPrice){
            this.setState({
                disableOffer:true,
            })
            pexios('/advertisement/offer/store','post',{adv_id:id,offer:this.state.offerPrice}).then(res=> {
                this.props.ad.offers.reverse()
                this.props.ad.offers.push(res.data.data)
                this.props.ad.offers.reverse()
                Toast.show({
                    text: 'پیشنهاد شما با موفقیت ثبت شد',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'success'
                })
                this.setState({
                    offered:true
                })
                this.forceUpdate()
            })
        }else{
            Toast.show({
                text: 'مبلغ ورودی نامعتبر است',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }
    }
    report(){
        pexios('/advertisement/report/store','post',{id:this.props.ad.id,text:this.state.selectedResult}).then(res=>{
            Toast.show({
                text: 'گزارش شما با موفقیت ثبت شد',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'success'
            })
            this.setState({reportLightBox:false})
        })
    }
}