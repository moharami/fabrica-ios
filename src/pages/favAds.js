import React, {Component} from 'react';
import {View, Text, Container, Button, Content, Item, Input, Form, Icon, Footer, FooterTab, Spinner} from 'native-base';
import {FlatList, Image, TouchableOpacity, AsyncStorage, BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { list,filter } from './../assets/styles';
import { pexios, url} from '../component/helper'
import Advertisement from "../component/advertisement";
import {CustomPicker} from "react-native-custom-picker";
import {shopL, single} from "../assets/styles";

export default class favAds extends Component{
    constructor(props){
        super(props)
        this.state={
            categories:[],
            lastPage:0,
            advertisements:[],
            page:1,
            adLoading:false,
            refreshing:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.getAdRequest();
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={list.container}>
                <Content style={{flex:1}}  contentContainerStyle={{flex: 1}}>
                    <View style={list.searchParent}>
                        <Button rounded full style={list.createButton} onPress={()=>Actions.createAd()}>
                            <Text style={list.createButtonText}>ایجاد آگهی</Text>
                        </Button>
                    </View>
                    <FlatList
                        ListFooterComponent={this.renderFooter.bind(this)}
                        data={this.state.advertisements}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={0.01}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                    />
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.myAds()}>
                            <Icon style={shopL.footerIcon} name="md-add" />
                            <Text style={shopL.footerText}>آگهی های من</Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>Actions.replace('list')}>
                            <Icon style={shopL.footerIcon} name="md-home" />
                            <Text style={shopL.footerText}>صفحه اصلی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    getAdRequest() {
        const { page } = this.state;
        if(!this.state.adLoading){
            this.setState({
                adLoading:true
            })
           pexios('/advertisement/liked-list?page='+page,'get').then(res=>{
                let adv = res.data.data.data;
                    this.setState(prevState=>{
                        return {
                            advertisements: page===1?adv:[...prevState.advertisements, ...adv],
                            page: res.data.data.current_page + 1,
                            lastPage: res.data.data.last_page,
                            refreshing : false
                        }
                    })
                this.setState({adLoading:false})
            })
        }
    }
    renderItem({ item }) {
        return <Advertisement ad={item.likable} myAd={false} />
    }
    handleLoadMore(){
        if(this.state.advertisements.length > 0 && this.state.page <= this.state.lastPage) {
            this.getAdRequest()
        }
    }
    handleRefresh(){
        this.setState({ page : 1 , refreshing : true } , () => {
            this.getAdRequest();
        })
    }
    renderFooter(){
        if(!this.state.adLoading) return null;

        return <View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
    }
}