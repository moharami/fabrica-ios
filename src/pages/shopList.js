import React, {Component} from 'react';
import {View, Text, Container, Button, Content, Item, Input, Form, Icon, Footer, FooterTab, Spinner} from 'native-base';
import {AsyncStorage, BackHandler, FlatList, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { shopL } from './../assets/styles';
import {pexios} from "../component/helper";
import {list, single} from "../assets/styles";
import Shop from '../component/shop'

export default class shopList extends Component{
    constructor(props) {
        super(props)
        this.state={
            shops:[],
            lastPage:0,
            page:1,
            adLoading:false,
            refreshing:false,
            login:false,
            q:''
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.getShopRequest();
        AsyncStorage.getItem('token',(error , result) => {
            if(result){
                this.setState({
                    login:true
                })
            }
        })
        console.log(this.props.id)
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={shopL.container}>
                <Content style={{flex:1}}>
                    <View style={shopL.searchParent}>
                        <Item regular style={shopL.searchItem}>
                            <Input placeholder='جستجو در فروشگاه ها' style={shopL.searchInput} onChangeText={(val)=>{this.setState({q:val})}} returnKeyType='search' onSubmitEditing={()=>{this.setState({page:1,shops:[],selectedCat:''},()=>{this.getShopRequest();})}} />
                            <Icon name={'md-search'} style={{position:'absolute',left:0}} onPress={()=>{this.setState({page:1,shops:[],selectedCat:''},()=>{this.getShopRequest();})}} />
                        </Item>
                    </View>
                    <FlatList
                        ListFooterComponent={this.renderFooter.bind(this)}
                        data={this.state.shops}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={0.01}
                    />
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.replace('list')}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>{if(this.state.login){Actions.createAd()}else{Actions.loginCheck()}}}>
                            <Icon style={shopL.footerIcon} name="md-add" />
                            <Text style={shopL.footerText}>ثبت آگهی خرید </Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>{if(this.state.login){Actions.createShop()}else{Actions.loginCheck()}}}>
                            <Icon style={shopL.footerIcon} name="card" />
                            <Text style={shopL.footerText}>ایجاد فروشگاه </Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    getShopRequest() {
        const { page } = this.state;
        if(!this.state.adLoading){
            this.setState({
                adLoading:true
            })
            pexios('/shop/show-all','post',{page:page,q:this.state.q,id:this.props.id}).then(res=>{
                let shop = res.data.data.data;
                this.setState(prevState=>{
                    return {
                        shops: page===1?shop:[...prevState.shops, ...shop],
                        page: res.data.data.current_page + 1,
                        lastPage: res.data.data.last_page,
                        refreshing : false
                    }
                })
                console.log(shop)
                this.setState({adLoading:false})
            })
        }
    }
    renderFooter(){
        if(!this.state.adLoading) return null;

        return <View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
    }
    renderItem({ item }) {
        return <Shop sh={item} />
    }

    handleLoadMore(){
        if(this.state.shops.length > 0 && this.state.page <= this.state.lastPage) {
            this.getShopRequest()
        }
    }
}