import React, {Component} from 'react';
import {View, Text, Container, Button, Content, Item, Input, Form, Spinner,Toast} from 'native-base';
import {AsyncStorage, BackHandler, Image} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { login } from './../assets/styles';
import {pexios} from '../component/helper'
import {signup} from "../assets/styles";


export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            code:null,
            disableBtn:false,
            timer:60,
            resendCode:false
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={login.container}>
                <Content>
                    <View style={login.logoParent}>
                        <Image source={require('../assets/images/logo.png')} style={login.logo} />
                    </View>
                    <Item regular style={login.inputCode}>
                        <Input style={login.inputCodeChild} placeholder='کد ارسالی را وارد نمایید' keyboardType="numeric" onChangeText={this.changeText.bind(this)} />
                    </Item>
                    <Button style={login.loginButton} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                        {
                            this.state.disableBtn?
                                <Spinner color='#fff' />
                                :
                                <Text style={login.loginButtonText}>ورود</Text>
                        }
                    </Button>
                    {
                        this.state.resendCode?
                            <Text style={{marginTop:20,textAlign:'center'}}>{this.state.timer}</Text>:
                            <Button transparent full style={{marginTop:20}} onPress={this.timer.bind(this)}>
                                <Text style={login.loginButtonText}>کد را دریافت نکردم</Text>
                            </Button>
                    }
                </Content>
            </Container>
        )
    }
    timer(){
        this.setState({
            resendCode:true
        })
        pexios('/resend','post',{mobile:this.props.mobile}).then(res=> {
            let timer = setInterval(()=>{
                if(this.state.timer == 0){
                    this.setState({
                        timer:60,
                        resendCode:false
                    })
                    clearInterval(timer);
                    console.log('end')
                }else{
                    this.setState({
                        timer:this.state.timer-1
                    })
                }
            },1000)
        })
    }
    changeText(value){
        this.setState({
            code:value
        })
    }
    submitForm(){
        this.setState({
            disableBtn:true,
        })
        pexios('/signin','post',{mobile:this.props.mobile,code:this.state.code}).then(res=>{
            AsyncStorage.setItem('token' , res.data );
            if(this.props.shop){
                Actions.reset('createShop')
            }else if(this.props.expert){
                Actions.reset('editExpert')
            }else{
                Actions.reset('list')
            }
        }).catch(er=>{
            Toast.show({
                text: 'کد ورودی صحیح نیست',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'center'},
                type:'danger',
                position:'top',
            })
            this.setState({
                disableBtn:false,
            })
        })
    }
}