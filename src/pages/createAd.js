import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    Spinner,
    Textarea,
    CheckBox, Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { createAd } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {Alert, BackHandler, Image, Switch} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

export default class CreateAd extends Component{
    constructor(props){
        super(props)
        this.state={
            priority:[
                {
                    title:'عادی',
                    key:'normal',
                },
                {
                    title:'فوری',
                    key:'immediate',
                },
                {
                    title:'ویژه',
                    key:'special',
                },
                {
                    title:'ویژه فوری',
                    key:'immediate-special',
                },
            ],
            type:[
                {
                    title:'فروش',
                    key:'sale',
                },
                {
                    title:'اجاره',
                    key:'rent',
                },
                {
                    title:'خرید',
                    key:'buy',
                },
            ],
            states:[],
            city:[],
            categories:[],
            subCategories:[],
            images:[],
            cityPicker:null,
            cityLoader:false,
            subCatPicker:null,
            subCatLoader:false,
            categoryItems:[],
            itemLoader:false,
            switch:[],
            datas:{},
            title:null,
            price:null,
            expiration_time:null,
            selectedType:null,
            selectedPriority:null,
            selectedCity:null,
            selectedCat:null,
            description:null,
            offer:false,
            disableBtn:false,
            requiredFields:[],
            priceText:''
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/data-states','get').then(res=>{
            this.setState({
                states:res.data
            })
        })
        pexios('/advertisement/category/list','get').then(res=>{
            this.setState({
                categories:res.data.data,
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={createAd.container}>
                    <Content style={{flex:1}}>
                        <Item regular style={this.state.requiredFields.includes(-6)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode}>
                            <Input style={createAd.inputCodeChild} placeholder='عنوان' onChangeText={(value)=>this.setState({title:value})}/>
                        </Item>
                        <Item regular style={this.state.requiredFields.includes(-5)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode}>
                            <Input style={createAd.inputCodeChild} placeholder='قیمت' keyboardType="numeric" onChangeText={(value)=>{this.toPersian(value);this.setState({price:value})}} />
                        </Item>
                        <Text style={createAd.price}>{this.state.priceText}</Text>
                        <Item regular style={this.state.requiredFields.includes(-1)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode}>
                            <Textarea rowSpan={5} placeholder='توضیحات' style={createAd.textArea}  onChangeText={(value)=>this.setState({description:value})} />
                        </Item>
                        <Item style={{flexDirection:'row-reverse',justifyContent:'flex-start',paddingTop:10,paddingBottom:10}}>
                            <Switch
                                value={this.state.offer}
                                onValueChange={() => this.setState({offer:!this.state.offer})}
                            />
                            <Text style={createAd.switchText}>پیشنهاد قیمت توسط مشتری</Text>
                        </Item>
                        <Item picker  style={this.state.requiredFields.includes(-3)?[{textAlign:'right'},{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'اولویت'}
                                options={this.state.priority}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.setState({selectedPriority:value.key}):this.setState({selectedPriority:null})}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker   style={this.state.requiredFields.includes(-4)?[{textAlign:'right'},{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'نوع'}
                                options={this.state.type}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.setState({selectedType:value.key}):this.setState({selectedType:null})}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#ff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker style={this.state.requiredFields.includes(-2)?[{textAlign:'right'},{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب استان'}
                                options={this.state.states}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.getCity(value.id):this.getCity(0)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        {
                            this.state.cityLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        {
                            this.state.cityPicker
                        }
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب دسته بندی'}
                                options={this.state.categories}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.getSubCat(value.id):this.getSubCat(0)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>

                        {
                            this.state.subCatLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        {
                            this.state.subCatPicker
                        }

                        {
                            this.state.itemLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        <View style={createAd.imgParent}>
                            {
                                this.state.images.map((img,index)=>{
                                    return <Button onPress={this.changeImage.bind(this,index)} key={index} transparent style={{width:100,height:100,marginTop:10}}><Image source={img} style={{width:100,height:100}} /></Button>
                                })
                            }
                        </View>
                        {
                            this.state.categoryItems.map((item)=>{
                                if(item.type=='text'){
                                    return <Item key={item.id} regular style={this.state.requiredFields.includes(item.id)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode}><Input style={createAd.inputCodeChild} placeholder={item.title} onChangeText={this.changeText.bind(this,item.id)} /></Item>
                                }
                                if(item.type == 'select'){
                                    return(
                                        <Item key={item.title} picker style={this.state.requiredFields.includes(item.id)?{textAlign:'right',borderColor:'red',borderWidth:1,borderStyle:'solid'}:{textAlign:'right'}}>
                                            <CustomPicker
                                                placeholder={item.title}
                                                options={JSON.parse(item.data)}
                                                getLabel={item => item}
                                                containerStyle={createAd.picker}
                                                onValueChange={value=>value!=null?this.state.datas[item.id]= value:this.state.datas[item.id]=''}
                                                fieldTemplateProps={{
                                                    containerStyle: {
                                                        borderBottomColor: '#fff',
                                                        textAlign:'right'
                                                    },
                                                    textStyle: {
                                                        color: '#454545',
                                                        fontSize:18,
                                                        textAlign:'right',
                                                        right:0,
                                                        position:'absolute',
                                                        fontFamily:'IRANYekanMobileFN'
                                                    },
                                                }}
                                                optionTemplateProps={{
                                                    containerStyle: {
                                                        backgroundColor: '#fff',
                                                        height: 60,
                                                        borderBottomWidth:0,
                                                    },
                                                    textStyle: {
                                                        color: '#343434',
                                                        right:15,
                                                        fontFamily:'IRANYekanMobileFN',
                                                        position:'absolute'
                                                    }}}
                                            />
                                        </Item>
                                    )
                                }
                                if(item.type == 'switch'){
                                    let index = this.state.switch.indexOf(this.state.switch.filter((sw)=>{if(sw.id == item.id){return true}})[0])
                                    return(
                                        <Item key={item.id} style={{flexDirection:'row-reverse',justifyContent:'flex-start',paddingTop:10,paddingBottom:10}}>
                                            <Switch
                                                value={this.state.switch[index].value}
                                                onValueChange={() => {this.state.switch[index].value=!this.state.switch[index].value;this.state.datas[item.id]=this.state.switch[index].value;this.forceUpdate();}}
                                            />
                                            <Text style={createAd.switchText}>{item.title}</Text>
                                        </Item>
                                    )
                                }
                                if(item.type == 'text-box'){
                                    return(
                                        <Item key={item.id} regular style={this.state.requiredFields.includes(item.id)?[createAd.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:createAd.inputCode}>
                                            <Textarea rowSpan={5} bordered placeholder={item.title} style={createAd.textArea}  onChangeText={this.changeText.bind(this,item.id)} />
                                        </Item>
                                    )
                                }
                            })
                        }

                        <Button style={createAd.Button} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={createAd.ButtonText}>ایجاد آگهی</Text>
                            }
                        </Button>
                    </Content>
                </View>
            </KeyboardAwareScrollView>
        )
    }
    changeText(id,value){
        this.state.datas[id]=value
    }
    selectCity(city_id){
        this.setState({
            selectedCity:city_id,
        })
    }
    changeImage(index,img){
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true
        }).then(image => {
            this.state.images[index]={uri:image.path,changed:true}
            this.forceUpdate()
        });
    }
    getCity(id){
        if(id==0){
            this.setState({cityPicker:null})
        }else {
            this.setState({cityPicker: null, cityLoader: true})
            pexios('/advertisement/data-cities/' + id, 'get').then(res => {
                this.setState({
                    city: res.data,
                }, () => {
                    this.setState({
                        cityLoader: false,
                        cityPicker: <Item picker style={{textAlign: 'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب شهر'}
                                options={this.state.city}
                                getLabel={item => item.title}
                                onValueChange={value => {
                                    value != null ? this.selectCity(value.city_id) : 0
                                }}
                                containerStyle={createAd.picker}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign: 'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize: 18,
                                        textAlign: 'right',
                                        right: 0,
                                        position: 'absolute',
                                        fontFamily: 'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth: 0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right: 15,
                                        fontFamily: 'IRANYekanMobileFN',
                                        position: 'absolute'
                                    }
                                }}
                            />
                        </Item>
                    })
                })

            })
        }
    }
    getSubCat(id){
        if(id==0){
            this.setState({subCatPicker:null})
        }else {
            this.setState({subCatPicker: null, subCatLoader: true})
            pexios('/advertisement/sub-category/' + id, 'get').then(res => {
                this.setState({
                    subCategories: res.data,
                }, () => {
                    this.setState({
                        subCatLoader: false,
                        subCatPicker: <Item picker style={{textAlign: 'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب زیر دسته '}
                                options={this.state.subCategories}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value => value != null ? this.getField(value.id) : 0}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign: 'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize: 18,
                                        textAlign: 'right',
                                        right: 0,
                                        position: 'absolute',
                                        fontFamily: 'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth: 0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right: 15,
                                        fontFamily: 'IRANYekanMobileFN',
                                        position: 'absolute'
                                    }
                                }}
                            />
                        </Item>
                    })
                })
            })
        }
    }
    getField(id){
        this.setState({itemLoader:true,selectedCat:id})
        pexios('/advertisement/data-category/'+id,'get').then(res=>{
            let catData = res.data
            for(let i = 0;i<catData.limit;i++){
                let img = {uri : url+'/images/add_image.png',changed:false}
                this.setState(prevState=>{
                    return {
                        images: [...prevState.images, img],
                    }
                })
            }
            res.data.items.filter((sw)=>{if(sw.type=='switch'){
                this.setState(prevState=> {
                    return {
                        switch: [...prevState.switch, {id: sw.id, value: sw.value==1?true:false}],
                    }
                })
                this.state.datas[sw.id]= sw.value
            }})
            res.data.items.filter((req)=>{if(req.required==1){
                this.state.datas[req.id]= ''
            }})
            this.setState({
                categoryItems:res.data.items,
                itemLoader:false,
                expiration_time:res.data.expiration_time
            })

        })
    }
    async submitForm(){
        this.setState({disableBtn:true,requiredFields:[]})
        let fd = object2formData({
            title:this.state.title,
            price:this.state.price,
            type:this.state.selectedType,
            priority:'normal',
            // priority:this.state.selectedPriority,
            city_id:this.state.selectedCity,
            category_id:this.state.selectedCat,
            description:this.state.description,
            expiration_time:this.state.expiration_time,
            offer:this.state.offer?1:0
        })
        if(!this.state.title || this.state.title==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -6]
                }
            })
        }
        if(!this.state.price || this.state.price==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -5]
                }
            })
        }
        if(!this.state.selectedType || this.state.selectedType==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -4]
                }
            })
        }
        if(!this.state.selectedPriority || this.state.selectedPriority==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -3]
                }
            })
        }
        if(!this.state.selectedCity || this.state.selectedCity==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -2]
                }
            })
        }
        if(!this.state.description || this.state.description==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -1]
                }
            })
        }
        for (let i in this.state.datas){
            fd.append(`datas[${i}]`,this.state.datas[i])
            if(this.state.datas[i] === ''){
                await this.state.categoryItems.map((cat)=>{
                    if(cat.id == i){
                        this.setState(prevState=>{
                            return{
                                requiredFields:[...prevState.requiredFields, cat.id]
                            }
                        })
                    }
                })
            }
        }
        if(this.props.shopId){
            fd.append('shop_id',this.props.shopId)
        }
        this.state.images.map((img)=>{
            if(img.changed){
                fd.append("image[]",{uri:img.uri,type:"image/jpg",name:"image[]"});
            }
        })
        if(!this.state.requiredFields.length){
            if(this.state.selectedPriority==='normal'){
                pexios('/advertisement/create','post',fd,true).then(res=>{
                    this.setState({disableBtn:false})
                    Actions.replace('myAds')
                }).catch(err=>{
                    this.setState({disableBtn:false})
                    Toast.show({
                        text: 'برای ثبت آگهی فیلد های اجباری را پر نمایید!',
                        textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                        duration: 3000,
                        type:'danger'
                    })
                })
            }else{
                pexios('/advertisement/create','post',fd,true).then(res=>{
                    this.setState({disableBtn:false})
                    pexios('/financial/create_invoice','post',{title:'advertisement',adv_id:res.data.data.id,type:this.state.selectedPriority}).then(res=>{
                        Alert.alert(
                            'ارور',
                            'نیازمند درگاه پرداخت',
                        );
                    })
                }).catch(err=>{
                    this.setState({disableBtn:false})
                    Toast.show({
                        text: 'برای ثبت آگهی فیلد های اجباری را پر نمایید!',
                        textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                        duration: 3000,
                        type:'danger'
                    })
                })
            }

        }else{
            this.setState({disableBtn:false})
            Toast.show({
                text: 'برای ثبت آگهی فیلد های اجباری را پر نمایید!',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }

    }
    toPersian(num){
        let NumToPersian = (function () {
            /**
             *
             * @type {string}
             */
            spliter = " و ";

            /**
             *
             * @type {string}
             */
            zero = "صفر";

            /**
             *
             * @type {*[]}
             */
            Letters = [
                ["", "یك", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"],
                ["ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده","بیست"],
                ["","","بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"],
                ["", "یكصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"],
                ['', " هزار ", " میلیون ", " میلیارد ", " بیلیون ", " بیلیارد ", " تریلیون ", " تریلیارد ",
                    " کوآدریلیون ", " کادریلیارد ", " کوینتیلیون ", " کوانتینیارد ", " سکستیلیون ", " سکستیلیارد ", " سپتیلیون ",
                    " سپتیلیارد ", " اکتیلیون ", " اکتیلیارد ", " نانیلیون ", " نانیلیارد ", " دسیلیون ", " دسیلیارد "]
            ];

            /**
             * Clear number and split to 3th sections
             * @param {*} num
             */
            function PrepareNumber(num){
                if(typeof num === "number"){
                    num = num.toString();
                }
                NumberLength = num.length%3;
                if(NumberLength === 1){
                    num = "00"+num;
                }else if(NumberLength === 2){
                    num = "0"+num;
                }
                //Explode to array
                return num.replace(/\d{3}(?=\d)/g, "$&*").split('*');
            }

            /**
             * Convert 3 numbers into letter
             * @param {*} num
             */
            function ThreeNumbersToLetter(num){
                //return zero
                if(parseInt(num) === 0){
                    return "";
                }
                parsedInt = parseInt(num);
                if(parsedInt < 10){
                    return Letters[0][parsedInt];
                }
                if(parsedInt <= 20){
                    return Letters[1][parsedInt-10];
                }
                if(parsedInt < 100){
                    one = parsedInt%10;
                    ten = (parsedInt-one)/10;
                    if(one > 0){
                        return Letters[2][ten] + spliter + Letters[0][one];
                    }
                    return Letters[2][ten];
                }
                one = parsedInt%10;
                hundreds = (parsedInt-parsedInt%100)/100;
                ten = (parsedInt-((hundreds*100)+one))/10;
                out = [Letters[3][hundreds]];
                SecendPart = ((ten*10)+one);
                if(SecendPart > 0){
                    if(SecendPart < 10){
                        out.push(Letters[0][SecendPart]);
                    }else if(SecendPart <= 20){
                        out.push(Letters[1][SecendPart-10]);
                    }else{
                        out.push(Letters[2][ten]);
                        if(one > 0){
                            out.push(Letters[0][one]);
                        }
                    }
                }
                return out.join(spliter);
            }

            /**
             * Main function
             */
            return function(num){
                //return zero
                if(parseInt(num) === 0){
                    return zero;
                }
                if(num.length > 66){
                    return "خارج از محدوده";
                }
                //Split to sections
                SplitedNumber = PrepareNumber(num);

                //Fetch Sections and convert
                funcout = [];
                SplitLength = SplitedNumber.length;
                for(i=0;i < SplitLength;i++){
                    SectionTitle = Letters[4][SplitLength-(i+1)];
                    converted = ThreeNumbersToLetter(SplitedNumber[i]);
                    if(converted !== ""){
                        funcout.push(converted+SectionTitle);
                    }
                }
                return funcout.join(spliter);
            };
        })();
        this.setState({
            priceText:NumToPersian(num)+' تومان '
        })
    }
}