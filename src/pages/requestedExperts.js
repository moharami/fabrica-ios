import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab,
    Spinner,
    Badge, Toast, Textarea
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Fabrika } from './../assets/styles';
import { pexios, url} from '../component/helper'
import {BackHandler, Image, TouchableOpacity} from "react-native";
import {createAd, list, shopL, single} from "../assets/styles";
import StarRating from 'react-native-star-rating';
export default class expertRequest extends Component{
    constructor(props) {
        super(props)
        this.state = {
            request:[],
            loaderRequest:false,
            starCount:[],
            comment:null,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/expert/list','get').then(res=>{
            this.setState({
                request:res.data.data,
            },async ()=>{
                await this.state.request.map((req)=>{
                    if(req.point){
                        this.state.starCount.push(req.point)
                    }else{
                        this.state.starCount.push(0)
                    }
                })
                this.setState({
                    loaderRequest:true
                })
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={Fabrika.container}>
                <Content style={{flex:1}}>
                    <View>
                        <Text style={Fabrika.expertTitle}>درخواست های کارشناسی:</Text>
                        {
                            this.state.loaderRequest?
                            this.state.request.length?
                            this.state.request.map((usr,index)=>{
                                return(
                                    <View key={usr.id}>
                                        <Item style={[Fabrika.expertParent,{flexWrap:'wrap'}]}>
                                            <Image source={{uri:usr.user.attachments?url+'/files?uid='+usr.user.attachments.uid+'&width=64&height=64':url+'/images/profile.png'}} style={Fabrika.expertUserPix} />
                                            <Text style={Fabrika.expertUserText}>{usr.user.fname} {usr.user.lname}</Text>
                                            <Text style={Fabrika.expertUserText}>{usr.user.mobile}</Text>
                                            <View>
                                                <Badge style={{width:100}} info><Text onPress={()=>Actions.single({ad:usr.advertisement,myAd:false})} style={Fabrika.btnText}> مشاهده </Text></Badge>
                                            </View>
                                            {
                                                usr.done===1?
                                                    <View style={{width:'100%'}}>
                                                        <View style={{marginTop:10,marginBottom:10,paddingLeft:40,paddingRight:40}}>
                                                            <StarRating
                                                                disabled={false}
                                                                maxStars={5}
                                                                rating={this.state.starCount[index]}
                                                                selectedStar={(rating) => this.onStarRatingPress(rating,index,usr.id)}
                                                                emptyStar={'ios-star-outline'}
                                                                fullStar={'ios-star'}
                                                                fullStarColor={'gold'}
                                                                emptyStarColor={'gold'}
                                                                iconSet={'Ionicons'}
                                                            />
                                                        </View>
                                                        <Item style={{backgroundColor:'#eee'}}>
                                                            <Textarea rowSpan={5} placeholder='نظر' style={createAd.textArea} onChangeText={(value)=>this.setState({comment:value})} value={this.state.comment}  />
                                                        </Item>
                                                        <Button style={Fabrika.commentButton} onPress={()=>this.submitComment(usr.id)}>
                                                            <Text style={Fabrika.commentButtonText}>ثبت نظر</Text>
                                                        </Button>
                                                    </View>
                                                    :null
                                            }
                                        </Item>
                                    </View>
                                )
                            }):<Text style={Fabrika.expertTitle}>موردی برای نمایش وجود ندارد</Text>:<View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
                        }

                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.myAds()}>
                            <Icon style={shopL.footerIcon} name="md-add" />
                            <Text style={shopL.footerText}>آگهی های من</Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>Actions.replace('list')}>
                            <Icon style={shopL.footerIcon} name="md-home" />
                            <Text style={shopL.footerText}>صفحه اصلی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    onStarRatingPress(rating,index,id) {
        pexios('/expert/point','post',{id:id,point:rating}).then(res=>{
            this.state.starCount[index]=rating
            this.forceUpdate()
        })
    }
    submitComment(id){
        if(this.state.comment){
            pexios('/expert/comment','post',{id:id,comment:this.state.comment}).then(res=>{
                Toast.show({
                    text: 'نظر شما با موفقیت ثبت شد',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'success'
                })
                this.setState({
                    comment:null
                })
            })
        }
    }

}