import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab, Toast, Spinner,
} from 'native-base';
import {AsyncStorage, BackHandler, Image, ImageBackground, ScrollView, TouchableOpacity, Dimensions} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Swiper from 'react-native-swiper';
import { single,filter } from './../assets/styles';
import moment from 'moment-jalaali';
import Communications from 'react-native-communications';
import {pexios, url ,priceFormat} from "../component/helper";
// import Share from 'react-native-share';
import HTML from 'react-native-render-html';

export default class Single extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        const { ad,myAd } = this.props;
        return(
            <Container style={single.container}>
                <Content style={{flex:1}}>
                    <View>
                        <Swiper style={{height:300}}  showsButtons={false}>
                            {
                                ad.attachmentsImage.length?
                                    ad.attachmentsImage.map((img)=>{
                                        return <View key={img.id}>
                                            <Lightbox underlayColor="white">
                                                <ImageBackground source={{uri :url+'/files?uid='+img.uid+'&width=400&height=300'}}  style={{width:'100%',height:300}}>
                                                    <View style={{position:'absolute',top:0,left:0,right:0,bottom:0,backgroundColor:'rgba(0,0,0,.2)'}}></View>
                                                </ImageBackground>
                                            </Lightbox>
                                        </View>
                                    }):
                                    <View>
                                        <ImageBackground source={require('../assets/images/no-image.png')}  style={{width:'100%',height:300}}>
                                            <View style={{position:'absolute',top:0,left:0,right:0,bottom:0,backgroundColor:'rgba(0,0,0,.2)'}}></View>
                                        </ImageBackground>
                                    </View>
                            }
                        </Swiper>
                        <TouchableOpacity  onPress={()=>Actions.pop()} style={{position:'absolute',left:25,top:15}}><Icon style={{color:'white'}} name={'ios-arrow-back'} /></TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.likeAd(ad.id)} style={{position:'absolute',right:60,top:15}}><Icon style={{color:'white'}} type="FontAwesome" name={this.state.likeIcon} /></TouchableOpacity>

                    </View>
                    <View style={single.whiteBox}>
                        <Text style={single.mainTitle}>{ad.title}</Text>
                    </View>
                    <View style={[single.whiteBox,{marginTop:5}]}>
                        <Text style={[single.title,single.bold]}>خلاصه:</Text>
                        <Text style={single.detailDesc}>
                            {ad.description}
                        </Text>
                    </View>
                    <View style={[single.whiteBox,{marginTop:5}]}>
                        <Text style={[single.title,single.bold]}>توضیحات:</Text>
                        <View>
                            <HTML html={ad.content} baseFontStyle={single.content} imagesMaxWidth={Dimensions.get('window').width} />
                        </View>
                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={() => Communications.text('09141014556')}>
                            <Icon style={single.footerIcon} name="ios-mail" />
                            <Text style={single.footerText}>ارسال پیامک</Text>
                        </Button>
                        <Button style={single.pageFooterButton} onPress={() => Communications.phonecall('09141014556', true)}>
                            <Icon style={single.footerIcon} name="ios-call" />
                            <Text style={single.footerText}>تماس تلفنی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    deleteAd(id){
        pexios('/advertisement/delete','post',{id:id}).then(res=>{
            Actions.myAds()
        })
    }
    likeAd(id){
        pexios('/advertisement/like','post',{id:id}).then(res=>{
            if(this.state.likeIcon=='bookmark'){
                this.setState({
                    likeIcon:'bookmark-o'
                })
            }else{
                this.setState({
                    likeIcon:'bookmark'
                })
            }

        })
    }
    offer(id){
        if(this.state.offerPrice){
            this.setState({
                disableOffer:true,
            })
            pexios('/advertisement/offer/store','post',{adv_id:id,offer:this.state.offerPrice}).then(res=> {
                this.props.ad.offers.reverse()
                this.props.ad.offers.push(res.data.data)
                this.props.ad.offers.reverse()
                Toast.show({
                    text: 'پیشنهاد شما با موفقیت ثبت شد',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'success'
                })
                this.setState({
                    offered:true
                })
                this.forceUpdate()
            })
        }else{
            Toast.show({
                text: 'مبلغ ورودی نامعتبر است',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }
    }
}