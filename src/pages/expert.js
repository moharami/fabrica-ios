import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    Badge,
    Footer,
    FooterTab,
    Toast, Spinner
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { expert } from './../assets/styles';
import {BackHandler, Image} from "react-native";
import {filter, list, shopL, single} from "../assets/styles";
import {pexios, url} from "../component/helper";
import moment from 'moment-jalaali';
import Communications from "react-native-communications";


export default class Expert extends Component{
    constructor(props){
        super(props)
        this.state={
            city:'',
            state:'',
            disabledBtn:false,
            comments:[],
            points:[],
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/city?id='+this.props.ex.city_id,'get').then(res=>{
            this.setState({
                city: res.data.title,
                state: res.data.state.title,
            })
        })
        pexios('/expert/comment/expert?id='+this.props.ex.id,'get').then(res=>{
            this.setState({
                comments:res.data.data
            })
        })
        pexios('/expert/get-point','post',{expert_id:this.props.ex.id}).then(res=>{
            for(let i =0;i<res.data.data.point;i++){
                this.state.points.push('md-star')
            }
            for(let i = 0;i<5-res.data.data.point;i++){
                this.state.points.push('md-star-outline')
            }
            this.forceUpdate()
        })

    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        const { ex , adId } = this.props;
        return(
            <Container style={expert.container}>
                <Content style={{flex:1}}>
                    <View style={expert.whiteBox}>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <View style={{justifyContent:'center',alignItems:'center'}}>
                                <Image source={{uri :url+'/files?uid='+ex.user.attachments.uid+'&width=150&height=150'}} style={expert.profilePicture} />
                                <Text style={[expert.userName,expert.bold]}>{ex.user.fname} {ex.user.lname}</Text>
                            </View>
                        </View>
                        <View style={expert.details}>
                            <View style={expert.detailBox}>
                                <Text style={expert.detailText}>تاریخ عضویت:</Text>
                                <Text style={expert.detailText}>{moment(ex.created_at).format('jYYYY/jM/jD')}</Text>
                            </View>
                            <View style={expert.detailBox}>
                                <Text style={expert.detailText}>استان:</Text>
                                <Text style={expert.detailText}>{this.state.state}</Text>
                            </View>
                            <View style={expert.detailBox}>
                                <Text style={expert.detailText}>شهر:</Text>
                                <Text style={expert.detailText}>{this.state.city}</Text>
                            </View>
                            <View style={expert.detailBox}>
                                <Text style={expert.detailText}>تماس همراه:</Text>
                                <Text style={expert.detailText}>{ex.user.mobile}</Text>
                            </View>
                            <View style={expert.detailBox}>
                                <Text style={expert.detailText}>ایمیل:</Text>
                                <Text style={expert.detailText}>{ex.user.email}</Text>
                            </View>
                            <View style={expert.detailBox}>
                                <Text style={expert.detailText}>امتیاز:</Text>
                                <View style={{flexDirection:'row'}}>
                                    {
                                            this.state.points.map((pt,index)=>{
                                                return <Icon key={index} name={pt} style={{color:'#eccc68',marginRight:2,marginLeft:2,}} />
                                            })
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={expert.buttonParent}>
                        <View style={{flex:1}}>
                            <Button block style={expert.reqButton} onPress={()=>this.requestExpert(adId)} disabled={this.state.disabledBtn}>
                                {
                                    this.state.disabledBtn?
                                        <Spinner color='#fff' />:
                                        <Text style={expert.buttonText}>درخواست کارشناسی</Text>
                                }
                            </Button>
                        </View>
                    </View>
                    <View style={expert.whiteBox}>
                        <Text style={[expert.title,expert.bold,{marginTop:20}]}>تخصص ها:</Text>
                        <View style={{flexDirection:'row-reverse',marginTop:10,flexWrap:'wrap'}}>
                            {
                                ex.categories.map((cat)=>{
                                    return <Text key={cat.id} style={expert.tags}>{cat.title}</Text>
                                })
                            }

                        </View>
                    </View>
                    <View style={expert.whiteBox}>
                        <Text style={[expert.title,expert.bold,{marginTop:20}]}>توضیحات:</Text>
                        <Text style={expert.desc}>{ex.description}</Text>
                    </View>
                    {
                        this.state.comments.length?
                            <View style={expert.whiteBox}>
                                {
                                    this.state.comments.map((cm)=>{
                                        return(
                                            <View key={cm.id}>
                                                <View style={expert.userComment}>
                                                    <Image source={{uri :cm.user.attachments?url+'/files?uid='+cm.user.attachments.uid+'&width=100&height=100':url+'/images/profile.png'}} style={expert.expertCommentImage} />
                                                    <Text style={expert.expertCommentName}>{cm.user.fname} {cm.user.lname}</Text>
                                                </View>
                                                <View style={expert.cmParent}>
                                                    <Text style={expert.cmText}>{cm.comment}</Text>
                                                </View>
                                            </View>
                                        )
                                    })
                                }

                            </View>
                            :null
                    }

                    <View style={expert.buttonParent}>
                        <View style={{flex:1}}>
                            <Button block style={expert.reqButton} onPress={()=>Actions.chat({id:ex.user.id})} >
                                <Text style={expert.buttonText}>چت با کارشناس</Text>
                            </Button>
                        </View>
                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[expert.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={expert.footerIcon} name="ios-arrow-back" />
                            <Text style={expert.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[expert.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={() => Communications.text(ex.user.mobile)}>
                            <Icon style={expert.footerIcon} name="ios-mail" />
                            <Text style={expert.footerText}>ارسال پیامک</Text>
                        </Button>
                        <Button style={expert.pageFooterButton} onPress={() => Communications.phonecall(ex.user.mobile,true)}>
                            <Icon style={expert.footerIcon} name="ios-call" />
                            <Text style={expert.footerText}>تماس تلفنی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    requestExpert(adId){
        this.setState({
            disabledBtn:true
        })
        pexios('/expert/assign','post',{adv_id:adId,expert_id:this.props.ex.id}).then(res=>{
           if(res.data.status == 400){
               Toast.show({
                   text: 'ظرفیت این کارشناس تکمیل است!',
                   textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                   duration: 3000,
                   type:'danger'
               })
           }else if(res.data.status == 409) {
               Toast.show({
                   text: 'شما قبلا برای این کارشناس درخواست داده اید!',
                   textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                   duration: 3000,
                   type:'danger'
               })
           }else{
               Toast.show({
                   text: 'درخواست شما با موفقیت ثبت شد',
                   textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                   duration: 3000,
                   type:'success'
               })
               this.setState({
                   disabledBtn:false
               })
           }
            this.setState({
                disabledBtn:false
            })

        })
    }
}