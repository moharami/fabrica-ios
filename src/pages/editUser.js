import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    CheckBox,
    Body,
    Spinner,
    Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { signup } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {BackHandler, Image, Switch} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {createAd} from "../assets/styles";

export default class editUser extends Component{
    constructor(props){
        super(props)
        this.state={
            image:{ uri : this.props.user.attachments?url+'/files?uid='+this.props.user.attachments.uid+'&width=100&height=100':url+'/images/profile.png'},
            states:[],
            city:[],
            fname:this.props.user.fname,
            lname:this.props.user.lname,
            email:this.props.user.email,
            selectedCity:null,
            switch:false,
            type:null,
            imageChanged:false,
            checkBox:[],
            selectedCat:[],
            cityPicker:null,
            cityLoader:false,
            disableBtn:false,
            requiredFields:[],
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/data-states','get').then(res=>{
            this.setState({
                states:res.data
            })
        })

    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={signup.container}>
                    <Content style={{flex:1}}>
                        <View style={{flexDirection:'row',justifyContent:'center'}} >
                            <Button onPress={this.changeImage.bind(this)} transparent style={{width:100,height:100}}>
                                <Image source={this.state.image} style={{width:100,height:100,borderRadius:50}} />
                            </Button>
                        </View>
                        <Item regular style={this.state.requiredFields.includes(-1)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='نام' onChangeText={this.changeText.bind(this,'fname')} value={this.state.fname}/>
                        </Item>
                        <Item regular style={this.state.requiredFields.includes(-2)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='نام خانوادگی' onChangeText={this.changeText.bind(this,'lname')} value={this.state.lname} />
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب استان'}
                                options={this.state.states}
                                getLabel={item => item.title}
                                containerStyle={signup.picker}
                                onValueChange={value=>value!=null?this.getCity(value.id):this.getCity(0)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        {
                            this.state.cityLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        {
                            this.state.cityPicker
                        }
                        <Button style={signup.loginButton} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={signup.loginButtonText}>ویرایش</Text>
                            }
                        </Button>
                    </Content>
                </View>
            </KeyboardAwareScrollView>
        )
    }
    changeImage(){
        ImagePicker.openPicker({
            width: 200,
            height: 200,
            cropping: true
        }).then(image => {
            this.setState({
                image:{uri:image.path},
                imageChanged:true
            })
        });
    }
    changeText(element,value){
        if(element=='fname'){
            this.setState({
                fname:value
            })
        }if(element=='lname'){
            this.setState({
                lname:value
            })
        }if(element=='email'){
            this.setState({
                email:value
            })
        }
    }
    selectCity(city_id){
        this.setState({
            selectedCity:city_id
        })
    }
    async submitForm(){
        this.setState({
            disableBtn:true,
            requiredFields:[]
        })
        if(!this.state.fname || this.state.fname==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -1]
                }
            })
        }
        if(!this.state.lname || this.state.lname==='' ){
            await this.setState(prevState=>{
                return{
                    requiredFields:[...prevState.requiredFields, -2]
                }
            })
        }
        let fd = object2formData({fname:this.state.fname,lname:this.state.lname,email:this.state.email,city_id:this.state.selectedCity})
        if(this.state.imageChanged){
            fd.append("avatar",{uri:this.state.image.uri,type:"image/jpg",name:"avatar"});
        }
        if(this.state.selectedCity){
            fd.append('city_id',this.state.selectedCity)
        }
        if(!this.state.requiredFields.length){
            pexios('/core/update','post',fd,true).then(res=>{
                Actions.reset('myFabrika')
            }).catch(err=>{
                this.setState({disableBtn:false})
                Toast.show({
                    text: 'برای تغییرات فیلد های اجباری را پر نمایید!',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'danger'
                })
            })
        }else{
            this.setState({disableBtn:false})
            Toast.show({
                text: 'برای تغییرات فیلد های اجباری را پر نمایید!',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'danger'
            })
        }

    }
    getCity(id){
        if(id==0){
            this.setState({cityPicker:null})
        }else {
            this.setState({cityPicker: null, cityLoader: true})
            pexios('/advertisement/data-cities/' + id, 'get').then(res => {
                this.setState({
                    city: res.data,
                }, () => {
                    this.setState({
                        cityLoader: false,
                        cityPicker: <Item picker style={{textAlign: 'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب شهر'}
                                options={this.state.city}
                                getLabel={item => item.title}
                                onValueChange={value => {
                                    value != null ? this.selectCity(value.city_id) : 0
                                }}
                                containerStyle={createAd.picker}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign: 'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize: 18,
                                        textAlign: 'right',
                                        right: 0,
                                        position: 'absolute',
                                        fontFamily: 'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth: 0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right: 15,
                                        fontFamily: 'IRANYekanMobileFN',
                                        position: 'absolute'
                                    }
                                }}
                            />
                        </Item>
                    })
                })

            })
        }
    }
}