import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab, Toast, Spinner,
} from 'native-base';
import {AsyncStorage, BackHandler, Image, ScrollView, TouchableOpacity} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { single,filter } from './../assets/styles';
import {pexios, url } from "../component/helper";
import {list, shopL} from "../assets/styles";

export default class Single extends Component{
    constructor(props){
        super(props)
        this.state={
            experts:[],
            login:true,
            user:null,
            points:{}
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/core/user','get').then(res=>{
            this.setState({
                user:res.data.data,
            })
        })
        AsyncStorage.getItem('token',(error , result) => {
            if(result){
                pexios('/expert/list-experts','post',{category_id:this.props.ad.category_id,city_id:this.props.ad.city_id}).then(res=>{
                    this.setState({
                        experts: res.data.data,
                        login:true
                    },()=>{
                        // this.state.experts.map((ex,index)=>{
                        //     pexios('/expert/get-point','post',{expert_id:ex.id}).then(res=>{
                        //         for(let i =0;i<res.data.data.point;i++){
                        //             this.state.points[index].push('md-star')
                        //         }
                        //         for(let i = 0;i<5-res.data.data.point;i++){
                        //             this.state.points[index].push('md-star-outline')
                        //         }
                        //         console.log(this.state.points[index])
                        //         this.forceUpdate()
                        //     })
                        // })
                    })
                })
            }
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={single.container}>
                <Content style={{flex:1}}>
                   <View style={single.expertHeader}><Text style={single.expertHeaderText}>درخواست کارشناس</Text></View>
                    {
                        this.state.experts.length?
                            this.state.experts.map((ex,index)=>{
                                return(
                                    <Button key={ex.id} transparent style={filter.expertParent} full onPress={()=>Actions.expert({ex:ex,adId:this.props.ad.id})}>
                                        <Image source={{uri :ex.user.attachments?url+'/files?uid='+ex.user.attachments.uid+'&width=100&height=100':url+'/images/profile.png'}} style={filter.expertImage} />
                                        <Text style={filter.expertName}>{ex.user.fname} {ex.user.lname}</Text>
                                        {/*<View style={{flexDirection:'row'}}>*/}
                                            {/*{*/}
                                                {/*this.state.points[index].map((pt,index)=>{*/}
                                                    {/*return <Icon key={index} name={pt} style={{color:'#eccc68',marginRight:2,marginLeft:2,}} />*/}
                                                {/*})*/}
                                            {/*}*/}
                                        {/*</View>*/}
                                    </Button>
                                )
                            }):
                            <Text style={filter.nothingText}>هیچ کارشناسی برای این آگهی وجود ندارد</Text>
                    }
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[list.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>{if(this.state.login){Actions.myFabrika()}else{Actions.loginCheck()}}}>
                            <Icon style={list.footerIcon} name="md-person" />
                            <Text style={list.footerText}>فابریکای من</Text>
                        </Button>
                        <Button style={list.pageFooterButton} onPress={()=>{if(this.state.login){Actions.editExpert({cats:this.state.user.expert.categories,desc:this.state.user.expert.description})}else{Actions.loginCheck({expert:true})}}}>
                            <Icon style={shopL.footerIcon} name="md-person" />
                            <Text style={shopL.footerText}>ثبت نام کارشناس</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}