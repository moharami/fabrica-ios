import React, {Component} from 'react';
import { Container , Header , View , Text , Left , Button , Right , Content , Form , Item , Icon , Input } from 'native-base';
import {splashSC} from "../assets/styles";
import {AsyncStorage, Image, StatusBar} from "react-native";
import { Actions } from 'react-native-router-flux';
export default class Splash extends Component {
    componentWillMount() {
        AsyncStorage.getItem('opened',(error , result) => {
            if(result){
                setTimeout(()=>{
                    Actions.replace('list')
                },2000)
            }else{
                setTimeout(()=>{
                    Actions.replace('help')
                },2000)
            }
        })
        // AsyncStorage.removeItem('opened')
    }
    render() {
        return (
            <Container style={splashSC.container}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content"/>
                <View style={{flex:.25}}></View>
                <View style={splashSC.logoParent}>
                    <Image source={require('../assets/images/logo.png')} />
                    <Text style={splashSC.mainTitle}>فابریکا</Text>
                    <Text style={splashSC.text}>لوازم فابریک صنعتی</Text>
                </View>
                <View style={splashSC.copyrightParent}>
                    <Text style={splashSC.version}>v1</Text>
                </View>
            </Container>
        )
    }
}