import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab,
    Spinner,
    Toast, ListItem, Left, Right, Radio, Badge
} from 'native-base';
import {FlatList, Image, TouchableOpacity, AsyncStorage, ImageBackground, BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { list, } from './../assets/styles';
import { pexios, url} from '../component/helper'
import Advertisement from "../component/advertisement";
import {createAd, filter, shopL, single} from "../assets/styles";
import Communications from "react-native-communications";

export default class shopAdList extends Component{
    constructor(props){
        super(props)
        this.state={
            categories:[],
            lastPage:0,
            advertisements:[],
            page:1,
            adLoading:false,
            refreshing:false,
            shop:null,
            reportLightBox:false,
            selectedResult:null,
            login:false,
            moreReport:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.getAdRequest();
        pexios('/shop/show','post',{id:this.props.id}).then(res=>{
            this.setState({
                shop:res.data.data
            })
            console.log(res.data.data)
        })
        AsyncStorage.getItem('token',(error , result) => {
            if(result){
                this.setState({
                    login:true
                })
            }
        })
        this.setState({
            isOwner:this.props.isOwner
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={list.container}>
                <Content style={{flex:1}}  contentContainerStyle={{flex: 1}}>
                    {this.state.reportLightBox?
                        <View style={filter.lightBox}>
                            <View style={filter.lightBoxContent}>
                                <Button transparent onPress={()=>this.setState({reportLightBox:false})}>
                                    <Icon name='md-close-circle' style={{ fontSize : 30 , color : '#34495e'}}/>
                                </Button>
                                <ListItem onPress={()=>this.setState({selectedResult:'صاحب آگهی در دسترس نیست',moreReport:false})}>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                        <Radio onPress={()=>this.setState({selectedResult:'صاحب آگهی در دسترس نیست',moreReport:false})} selected={this.state.selectedResult=='صاحب آگهی در دسترس نیست'?true:false} />
                                        <Text style={[filter.ButtonText,{marginRight:10}]}>صاحب آگهی در دسترس نیست</Text>
                                    </View>
                                </ListItem>
                                <ListItem onPress={()=>this.setState({selectedResult:'محصول یا خدمت آگهی شده دیگر موجود نیست',moreReport:false})}>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                        <Radio onPress={()=>this.setState({selectedResult:'محصول یا خدمت آگهی شده دیگر موجود نیست',moreReport:false})} selected={this.state.selectedResult=='محصول یا خدمت آگهی شده دیگر موجود نیست'?true:false} />
                                        <Text style={[filter.ButtonText,{marginRight:10}]}>محصول یا خدمت آگهی شده دیگر موجود نیست</Text>
                                    </View>
                                </ListItem>
                                <ListItem onPress={()=>this.setState({selectedResult:'گروه بندی نادرست است',moreReport:false})}>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                        <Radio onPress={()=>this.setState({selectedResult:'گروه بندی نادرست است',moreReport:false})} selected={this.state.selectedResult=='گروه بندی نادرست است'?true:false} />
                                        <Text style={[filter.ButtonText,{marginRight:10}]}>گروه بندی نادرست است</Text>
                                    </View>
                                </ListItem>
                                <ListItem onPress={()=>this.setState({selectedResult:'اطلاعات تماس نادرست است',moreReport:false})}>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                        <Radio onPress={()=>this.setState({selectedResult:'اطلاعات تماس نادرست است',moreReport:false})} selected={this.state.selectedResult=='اطلاعات تماس نادرست است'?true:false} />
                                        <Text style={[filter.ButtonText,{marginRight:10}]}>اطلاعات تماس نادرست است</Text>
                                    </View>
                                </ListItem>
                                <ListItem onPress={()=>this.setState({moreReport:true})}>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start',width:'100%'}}>
                                        <Radio onPress={()=>this.setState({moreReport:true})} selected={this.state.moreReport} />
                                        <Text style={[filter.ButtonText,{marginRight:10}]}>موارد دیگر</Text>
                                    </View>
                                </ListItem>
                                {
                                    this.state.moreReport?
                                        <ListItem>
                                            <Textarea rowSpan={5} placeholder='توضیحات' style={createAd.textArea}  onChangeText={(value)=>this.setState({selectedResult:value})} />
                                        </ListItem>:null
                                }
                                <View style={{flexDirection:'row',justifyContent:'center',marginTop:20,marginBottom:20}}>
                                    <Button style={filter.Button} onPress={this.report.bind(this)} ><Text style={filter.ButtonText}> ارسال </Text></Button>
                                </View>
                            </View>
                        </View>:null
                    }
                    {
                        this.state.shop?
                            <View  style={list.shopBackground}>
                                <View style={{padding:20}}>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'flex-start'}}>
                                        {
                                            this.state.shop.address==''?
                                                <Image style={{height:64,width:64}} source={{uri:this.state.shop.user.attachments? url+'/files?uid='+sh.user.attachments.uid+'&width=64&height=64':url+'/images/profile.png'}} />:
                                                <Image style={{height:64,width:64}} source={{uri:this.state.shop.attachmentsImage.filter((img)=>{if(img.filename=='logo')return true}).length? url+'/files?uid='+this.state.shop.attachmentsImage.filter((img)=>{if(img.filename=='logo')return true})[0].uid+'&width=64&height=64':url+'/images/profile.png'}} />
                                        }
                                        <View style={{marginRight:20}}>
                                            <Text style={[list.shopTitle,list.bold]}>{this.state.shop.title}</Text>
                                            <Text style={list.shopText}>آدرس:  {this.state.shop.address}</Text>
                                            <Text style={list.shopText}>
                                                تلفن تماس:  {this.state.shop.tell?this.state.shop.tell:this.state.shop.mobile}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'space-between',marginTop:20}}>
                                        <View>
                                            <View style={{flexDirection:'row-reverse',justifyContent:'flex-start'}}>
                                                <Text style={list.starText}>اعتبار:</Text>
                                                {
                                                    this.state.shop.type=='gold'?
                                                        <View style={list.detail}>
                                                            <Text style={[list.detailText,{textAlign:'right'}]}>اعتبار</Text>
                                                            <Badge style={{backgroundColor:'#eccc68'}}><Text style={list.shopStarText}>اعتبار طلایی</Text></Badge>
                                                        </View>
                                                        :
                                                        this.state.shop.type=='silver'?
                                                            <View style={list.detail}>
                                                                <Text style={[list.detailText,{textAlign:'right'}]}>اعتبار</Text>
                                                                <Badge style={{backgroundColor:'#a4b0be'}}><Text style={list.shopStarText}>اعتبار نقره ای</Text></Badge>
                                                            </View>
                                                            :
                                                            <Text style={list.starText}>بدون اعتبار</Text>
                                                }
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View> :null
                    }

                    <View style={single.buttonParent}>
                        <View style={{width:'100%'}}>
                            <Button block style={single.chatButton} onPress={()=>this.state.login?this.setState({reportLightBox:true}):Actions.loginCheck()}>
                                <Text style={single.chatButtonText}>گزارش خطا</Text>
                            </Button>
                        </View>
                    </View>
                    <FlatList
                        ListFooterComponent={this.renderFooter.bind(this)}
                        data={this.state.advertisements}
                        renderItem={this.renderItem.bind(this)}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={0.01}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                    />
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={() => Communications.text(this.state.shop.mobile)}>
                            <Icon style={single.footerIcon} name="ios-mail" />
                            <Text style={single.footerText}>ارسال پیامک</Text>
                        </Button>
                        <Button style={single.pageFooterButton} onPress={() => Communications.phonecall(this.state.shop.tell?this.state.shop.tell:this.state.shop.mobile, true)}>
                            <Icon style={single.footerIcon} name="ios-call" />
                            <Text style={single.footerText}>تماس تلفنی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    getAdRequest() {
        const { page } = this.state;
        if(!this.state.adLoading){
            this.setState({
                adLoading:true
            })
            pexios('/shop/show-adv','post',{page:page,id:this.props.id}).then(res=>{
                let adv = res.data.data.data;
                this.setState(prevState=>{
                    return {
                        advertisements: page===1?adv:[...prevState.advertisements, ...adv],
                        page: res.data.data.current_page + 1,
                        lastPage: res.data.data.last_page,
                        refreshing : false
                    }
                })
                this.setState({adLoading:false})
            })
        }
    }
    renderItem({ item }) {
        return <Advertisement ad={item} myAd={this.state.isOwner} />
    }
    handleLoadMore(){
        if(this.state.advertisements.length > 0 && this.state.page <= this.state.lastPage) {
            this.getAdRequest()
        }
    }
    handleRefresh(){
        this.setState({ page : 1 , refreshing : true } , () => {
            this.getAdRequest();
        })
    }
    renderFooter(){
        if(!this.state.adLoading) return null;

        return <View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
    }
    report(){
        pexios('/shop/report/store','post',{id:this.props.id,text:this.state.selectedResult}).then(res=>{
            Toast.show({
                text: 'گزارش شما با موفقیت ثبت شد',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'success'
            })
            this.setState({reportLightBox:false})
        })
    }
}