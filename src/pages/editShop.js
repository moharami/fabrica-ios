import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    CheckBox,
    Body,
    Textarea,
    Spinner
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { signup } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {BackHandler, Image, Switch} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {createAd} from "../assets/styles";

export default class editShop extends Component{
    constructor(props){
        super(props)
        this.state={
            image:{ uri : this.props.sh.attachmentsImage.filter((img)=>{if(img.filename=="cover")return true}).length?url+'/files?uid='+this.props.sh.attachmentsImage.filter((img)=>{if(img.filename=='cover')return true})[0].uid+'&width=300&height=100':url+'/images/shopBg.png'},
            logo:{ uri : this.props.sh.attachmentsImage.filter((img)=>{if(img.filename=="logo")return true}).length?url+'/files?uid='+this.props.sh.attachmentsImage.filter((img)=>{if(img.filename=='logo')return true})[0].uid+'&width=64&height=64':url+'/images/profile.png'},
            states:[],
            city:[],
            name:this.props.sh.title,
            mobile:this.props.sh.mobile,
            tell:this.props.sh.tell,
            selectedCity:this.props.sh.city_id,
            imageChanged:false,
            businessNumber:this.props.sh.business_licence,
            address:this.props.sh.address,
            type:[
                {title:'ملک شخصی',value:1},
                {title:'ملک اجاره ای',value:0},
            ],
            selectedType:this.props.sh.owner,
            logoChanged:false,
            disableBtn:false,
            checkBox:[],
            categories:[],
            selectedCat:[],
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/data-states','get').then(res=>{
            this.setState({
                states:res.data
            })
        })
        pexios('/advertisement/category/list','get').then(res=>{
            this.setState({
                categories:res.data.data,
            },async ()=>{
                await this.state.categories.map((cat)=>{
                    cat.subs.map((sub)=>{
                        this.setState(prevState=> {
                            return {
                                checkBox: [...prevState.checkBox, {id:sub.id,value:false}],
                            }
                        })
                    })
                })
                this.props.sh.categories.map(prop=>{
                    this.state.checkBox.map((cb,index)=>{
                        if(cb.id == prop.id){
                            this.state.checkBox[index]={id:cb.id,value:true}
                            this.state.selectedCat.push(cb.id)
                            this.forceUpdate()
                        }
                    })
                })
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={signup.container}>
                    <View style={{flex:1}}>
                        <Text style={signup.imgTitle}>تصویر لوگو</Text>
                        <View style={{flexDirection:'row',justifyContent:'center',marginTop:10}} >
                            <Button onPress={this.changeLogo.bind(this)} transparent style={{width:64,height:64}}>
                                <Image source={this.state.logo} style={{width:64,height:64,borderRadius:50}} />
                            </Button>
                        </View>
                        <Text style={[signup.imgTitle,{marginTop:10}]}>تصویر کاور</Text>
                        <View style={{flexDirection:'row',justifyContent:'center',marginTop:10}} >
                            <Button onPress={this.changeImage.bind(this)} transparent style={{height:100}} full>
                                <Image source={this.state.image} style={{width:'100%',height:100}} />
                            </Button>
                        </View>
                        <Item regular style={signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='نام فروشگاه' onChangeText={(val)=>{this.setState({name:val})}} value={this.state.name} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='شماره موبایل' keyboardType="numeric" onChangeText={(val)=>{this.setState({mobile:val})}} value={this.state.mobile} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='شماره ثابت فروشگاه' keyboardType="numeric" onChangeText={(val)=>{this.setState({tell:val})}} value={this.state.tell} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Input style={signup.inputCodeChild} placeholder='شماره پروانه کسب' onChangeText={(val)=>{this.setState({businessNumber:val})}} value={this.state.businessNumber} />
                        </Item>
                        <Item regular style={signup.inputCode}>
                            <Textarea rowSpan={5} bordered placeholder='آدرس فروشگاه' style={signup.textArea} onChangeText={(val)=>{this.setState({address:val})}} value={this.state.address} />
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'نوع ملک'}
                                options={this.state.type}
                                getLabel={item => item.title}
                                containerStyle={signup.picker}
                                onValueChange={value=>{this.setState({selectedType:value.value})}}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب استان'}
                                options={this.state.states}
                                getLabel={item => item.title}
                                containerStyle={signup.picker}
                                onValueChange={value=>this.getCity(value.id)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب شهر'}
                                options={this.state.city}
                                getLabel={item => item.title}
                                onValueChange={value=>{this.selectCity(value.city_id)}}
                                containerStyle={signup.picker}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <Text style={signup.expertTitle}>دسته بندی هایی که در آن تخصص دارید را انتخاب نمایید</Text>
                        <View style={signup.catParent}>
                            {this.state.categories.map((cat)=>{
                                return(
                                    <View key={cat.id} style={{marginTop:15,borderBottomWidth:1,borderBottomColor:'#eee',padding:5}}>
                                        <Text style={signup.catTitle}>{cat.title}</Text>
                                        <View style={signup.subCatParent}>
                                            {
                                                cat.subs.map((sub)=>{
                                                    let index = this.state.checkBox.indexOf(this.state.checkBox.filter((cb)=>{if(cb.id == sub.id){return true}})[0])
                                                    return (
                                                        <View key={sub.id} style={{width:'50%',paddingRight:5,paddingLeft:5}}>
                                                            <Item style={{borderBottomWidth:0}} onPress={()=>this.selectCat(index,sub.id)}>
                                                                <Body>
                                                                <Text>{sub.title}</Text>
                                                                </Body>
                                                                {
                                                                    this.state.checkBox.length?
                                                                        <CheckBox checked={this.state.checkBox[index].value}/> :null
                                                                }

                                                            </Item>
                                                        </View>
                                                    )
                                                })
                                            }
                                        </View>
                                    </View>
                                )
                            })}
                        </View>
                        <Button style={signup.loginButton} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={signup.loginButtonText}>ایجاد فروشگاه</Text>
                            }
                        </Button>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
    changeImage(){
        ImagePicker.openPicker({
            width: 300,
            height: 100,
            cropping: true
        }).then(image => {
            this.setState({
                image:{uri:image.path},
                imageChanged:true
            })
        });
    }
    changeLogo(){
        ImagePicker.openPicker({
            width: 64,
            height: 64,
            cropping: true
        }).then(image => {
            this.setState({
                logo:{uri:image.path},
                logoChanged:true
            })
        });
    }
    selectCity(city_id){
        this.setState({
            selectedCity:city_id
        })
    }
    selectCat(index,id){
        if(this.state.checkBox[index].value){
            let catIndex = this.state.selectedCat.indexOf(id)
            if(catIndex>=0)
                this.state.selectedCat.splice(catIndex, 1)
        }else{
            this.state.selectedCat.push(id)
        }
        this.state.checkBox[index].value=!this.state.checkBox[index].value;
        this.forceUpdate();
    }
    submitForm(){
        this.setState({
            disableBtn:true,
        })
        let fd = object2formData({id:this.props.sh.id,title:this.state.name,tell:this.state.tell,mobile:this.state.mobile,city_id:this.state.selectedCity,owner:this.state.selectedType,address:this.state.address,business_licence:this.state.businessNumber})
        if(this.state.imageChanged){
            fd.append("cover",{uri:this.state.image.uri,type:"image/jpg",name:"cover"});
        }
        if(this.state.logoChanged){
            fd.append("logo",{uri:this.state.logo.uri,type:"image/jpg",name:"logo"});
        }
        for (let i in this.state.selectedCat){
            fd.append(`category_id[${i}]`,this.state.selectedCat[i])
        }
        pexios('/shop/update','post',fd,true).then(res=>{
            Actions.reset('myFabrika')
        }).catch(err=>{
            this.setState({disableBtn:false})
            console.log(err.response)
        })
    }
    getCity(id){
        pexios('/advertisement/data-cities/'+id,'get').then(res=>{
            this.setState({
                city:res.data
            })
        })
    }
}