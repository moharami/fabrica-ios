import React, {Component} from 'react';
import {View, Text, Container, Button, Content, Item, Input, Form, Icon, Footer, FooterTab, Spinner} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Fabrika } from './../assets/styles';
import { pexios, url} from '../component/helper'
import {Image, ImageBackground, TouchableOpacity, AsyncStorage, BackHandler} from "react-native";
import {shopL, single} from "../assets/styles";
import ShopItems from '../component/shopItems'
export default class myFabrika extends Component{
    constructor(props) {
        super(props)
        this.state = {
            user:null,
            loader:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/core/user','get').then(res=>{
            this.setState({
                user:res.data.data,
                loader:true,
            },()=>{
                console.log(this.state.user)
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={Fabrika.container}>
                <Content style={{flex:1}}>
                    <ImageBackground style={Fabrika.imageParent} source={{ uri : url+'/images/shopBg.png'}} >
                        <View>
                            {
                                this.state.loader?
                                    <View>
                                        <Image source={{ uri : this.state.user.attachments?url+'/files?uid='+this.state.user.attachments.uid+'&width=150&height=150':url+'/images/profile.png'}} style={Fabrika.img} />
                                        <Text style={Fabrika.userText}>{this.state.user.fname} {this.state.user.lname}</Text>
                                    </View>
                                    :null
                            }
                        </View>
                    </ImageBackground>
                    {
                        this.state.loader?
                            <View>
                                <Item style={{marginTop:20}}>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.chatUser()}>
                                        <Icon style={Fabrika.icons} name={'md-mail'}/>
                                        <Text style={Fabrika.text}>پیام خصوصی</Text>
                                    </TouchableOpacity>
                                </Item>
                                <Item>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.editUser({user:this.state.user})}>
                                        <Icon style={Fabrika.icons} name={'md-person'}/>
                                        <Text style={Fabrika.text}>ویرایش مشخصات</Text>
                                    </TouchableOpacity>
                                </Item>
                                <Item>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.favAds()}>
                                        <Icon style={Fabrika.icons} type="FontAwesome" name={'bookmark'}/>
                                        <Text style={Fabrika.text}>آگهی های مورد علاقه</Text>
                                    </TouchableOpacity>
                                </Item>
                                <Item>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.requestedExperts()}>
                                        <Icon style={Fabrika.icons} name={'md-list'}/>
                                        <Text style={Fabrika.text}>لیست کارشناسی ها</Text>
                                    </TouchableOpacity>
                                </Item>
                            </View>
                            :null
                    }
                    {
                        this.state.loader?
                        this.state.user.isExpert?
                            <View>
                                <Item>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.editExpert({cats:this.state.user.expert.categories,desc:this.state.user.expert.description,NI:this.state.user.expert.national_code,history:this.state.user.expert.history,birthday:this.state.user.expert.birthday})}>
                                        <Icon style={Fabrika.icons} type="FontAwesome" name={'edit'}/>
                                        <Text style={Fabrika.text}>ویرایش مشخصات کارشناس</Text>
                                    </TouchableOpacity>
                                </Item>
                                <Item>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.expertRequest({id:this.state.user.id})}>
                                        <Icon style={Fabrika.icons} name={'md-list'}/>
                                        <Text style={Fabrika.text}>لیست درخواست های کارشناسی</Text>
                                    </TouchableOpacity>
                                </Item>
                            </View>:
                            <Item>
                                <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.editExpert()}>
                                    <Icon style={Fabrika.icons} name={'md-man'}/>
                                    <Text style={Fabrika.text}>ثبت نام کارشناس</Text>
                                </TouchableOpacity>
                            </Item>:null

                    }
                    <Item>
                        <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.createShop()}>
                            <Icon style={Fabrika.icons} type="FontAwesome" name={'shopping-cart'}/>
                            <Text style={Fabrika.text}>ساخت فروشگاه</Text>
                        </TouchableOpacity>
                    </Item>
                    {
                        this.state.loader?
                            this.state.user.shop.map((shop)=>{
                                return(
                                    <ShopItems shop={shop} key={shop.id} />
                                )
                            })
                            :null
                    }
                    <Item>
                        <TouchableOpacity style={Fabrika.buttons} onPress={this.logout}>
                            <Icon style={Fabrika.icons} name={'md-power'}/>
                            <Text style={Fabrika.text}>خروج</Text>
                        </TouchableOpacity>
                    </Item>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.myAds()}>
                            <Icon style={shopL.footerIcon} name="md-add" />
                            <Text style={shopL.footerText}>آگهی های من</Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>Actions.replace('list')}>
                            <Icon style={shopL.footerIcon} name="md-home" />
                            <Text style={shopL.footerText}>صفحه اصلی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    logout(){
        AsyncStorage.removeItem('token')
        Actions.reset('splash')
    }
}