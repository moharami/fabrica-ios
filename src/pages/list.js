import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Icon,
    Footer,
    FooterTab,
    Spinner, Switch,
} from 'native-base';
import {
    FlatList,
    Image,
    TouchableOpacity,
    AsyncStorage,
    StatusBar,
    BackHandler,
    Animated,
    ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { list,filter } from './../assets/styles';
import {pexios,url} from '../component/helper'
import Advertisement from "../component/advertisement";
import {CustomPicker} from "react-native-custom-picker";
import {createAd} from "../assets/styles";

export default class List extends Component{
    constructor(props){
        super(props)
        this.state={
            subCatPicker:null,
            subCatLoader:false,
            itemLoader:false,
            switch:[],
            categories:[],
            lastPage:0,
            advertisements:[],
            page:1,
            adLoading:false,
            refreshing:false,
            cat_parent:null,
            subCategories:[],
            noAd:null,
            priority:[
                {
                    title:'عادی',
                    key:'normal',
                },
                {
                    title:'فوری',
                    key:'immediate',
                },
                {
                    title:'ویژه',
                    key:'special',
                },
                {
                    title:'ویژه فوری',
                    key:'immediate-special',
                },
            ],
            orderBy:[
                {
                    title:'صعودی قیمت',
                    key:'priceAsc',
                },
                {
                    title:'نزولی قیمت',
                    key:'priceDesc',
                },
                {
                    title:'صعودی تاریخ',
                    key:'idAsc',
                },
                {
                    title:'نزولی تاریخ',
                    key:'idDesc',
                }
            ],
            lightBox:false,
            withoutFilter:true,
            filter:{
                pricemin:null,
                pricemax:null,
                orderby:null,
                priority:null,
                categoryId:null,
            },
            search:{
                state_id:null,
                city_id:null,
                datas:{},
                searchKey:null
            },
            showSuggestion:false,
            login:null,
            searchInput:null,
            categoryItems:[],
            exit:false,
        }
        this.suggestValue = new Animated.Value(0)
        this.offset = 0
        this.animateTime=true
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
        AsyncStorage.getItem('token',(error , result) => {
            if(result){
                this.setState({
                    login:true
                })
            }
        })
        pexios('/advertisement/category/list','get').then(res=>{
            this.setState({
                categories:res.data.data,
            })
            if(this.props.search){
                console.log(this.props.search.datas)
                this.setState(prevState=>{
                    return{
                        search:{
                            state_id:this.props.search.state_id,
                            city_id:this.props.search.city_id,
                            datas:this.props.search.datas,
                            searchKey:this.props.search.searchKey
                        },
                        filter:{
                            ...prevState.filter,
                            categoryId:this.props.search.selectedSubCat,
                        },
                        withoutFilter:false,
                        cat_parent:this.props.search.selectedCat
                    }

                },()=>{
                    this.getAdRequest();
                })
            }else{
                AsyncStorage.getItem('filter',(error,result)=>{
                    let json = JSON.parse(result)
                    if(json){
                        this.setState({
                            cat_parent:json.cat_parent,
                            filter:json.filter,
                            search:json.search,
                            withoutFilter:false,

                        },()=>this.getAdRequest())
                    }else{
                        this.getAdRequest();
                    }
                })

            }
        })
    }
    handleBackButton(){
        if(this.state.exit){
            BackHandler.exitApp()
            return true;
        }else{
            this.setState({exit:true},()=>{
                setTimeout(()=>{
                    this.setState({exit:false})
                },1000)
            })
        }

    }
    render(){
        const suggest = this.suggestValue.interpolate({
            inputRange:[0,1],
            outputRange:[40,0]
        })
        return(
            <Container style={list.container}>
                <StatusBar backgroundColor="rgba(0,0,0,.1)" barStyle="dark-content"/>
                {this.state.lightBox?
                    <View style={filter.lightBox}>
                        <View style={[filter.lightBoxContent,{flex:.8}]}>
                            <ScrollView>
                            <Button transparent style={{ position: 'absolute', top : 0 , left : 0}} onPress={()=>this.setState({lightBox:false})}>
                                <Icon name='md-close-circle' style={{ fontSize : 30 , color : '#34495e'}}/>
                            </Button>
                            <View style={{flexDirection:'row',marginTop:50,paddingRight:5,paddingLeft:5}}>
                                <View style={filter.priceInputParent}>
                                    <Item regular>
                                        <Input style={filter.priceInput} keyboardType="numeric" placeholder='حداقل قیمت (تومان)' onChangeText={(value)=>this.setState(prevState => {return {filter : {...prevState.filter,pricemin : value}}})} />
                                    </Item>
                                </View>
                                <View style={filter.priceInputParent}>
                                    <Item regular>
                                        <Input style={filter.priceInput} keyboardType="numeric" placeholder='حداکثر قیمت (تومان)' onChangeText={(value)=>this.setState(prevState => {return {filter : {...prevState.filter,pricemax : value}}})} />
                                    </Item>
                                </View>
                            </View>
                            <View style={{flexDirection:'row',marginTop:15,paddingRight:5,paddingLeft:5}}>
                                <View style={filter.priceInputParent}>
                                    <Item picker style={{textAlign:'right',borderBottomWidth:0}}>
                                        <CustomPicker
                                            placeholder={'اولویت'}
                                            options={this.state.priority}
                                            getLabel={item => item.title}
                                            containerStyle={filter.smallPicker}
                                            onValueChange={value=>value!=null?this.setState(prevState => {return {filter : {...prevState.filter,priority : value.key}}}):this.setState(prevState => {return {filter : {...prevState.filter,priority : null}}})}
                                            fieldTemplateProps={{
                                                containerStyle: {
                                                    borderBottomColor: '#fff',
                                                    textAlign:'right',
                                                    marginTop:0,
                                                    borderBottomWidth:0
                                                },
                                                textStyle: {
                                                    color: '#454545',
                                                    fontSize:14,
                                                    textAlign:'right',
                                                    right:0,
                                                    position:'absolute',
                                                    fontFamily:'IRANYekanMobileFN'
                                                },
                                            }}
                                            optionTemplateProps={{
                                                containerStyle: {
                                                    backgroundColor: '#fff',
                                                    height: 40,
                                                    borderBottomWidth:0,
                                                },
                                                textStyle: {
                                                    color: '#343434',
                                                    right:15,
                                                    fontFamily:'IRANYekanMobileFN',
                                                    position:'absolute'
                                                }}}
                                        />
                                    </Item>
                                </View>
                                <View style={filter.priceInputParent}>
                                    <Item picker style={{textAlign:'right',borderBottomWidth:0}}>
                                        <CustomPicker
                                            placeholder={'مرتب سازی بر اساس'}
                                            options={this.state.orderBy}
                                            getLabel={item => item.title}
                                            onValueChange={value=>value!=null?this.setState(prevState => {return {filter : {...prevState.filter,orderby : value.key}}}):this.setState(prevState => {return {filter : {...prevState.filter,orderby : null}}})}
                                            containerStyle={filter.smallPicker}
                                            fieldTemplateProps={{
                                                containerStyle: {
                                                    borderBottomColor: '#fff',
                                                    textAlign:'right',
                                                    marginTop:0,
                                                    borderBottomWidth:0
                                                },
                                                textStyle: {
                                                    color: '#454545',
                                                    fontSize:14,
                                                    textAlign:'right',
                                                    right:0,
                                                    position:'absolute',
                                                    fontFamily:'IRANYekanMobileFN'
                                                },
                                            }}
                                            optionTemplateProps={{
                                                containerStyle: {
                                                    backgroundColor: '#343434',
                                                    height: 40,
                                                    borderBottomWidth:0,
                                                },
                                                textStyle: {
                                                    color: '#fff',
                                                    right:15,
                                                    fontFamily:'IRANYekanMobileFN',
                                                    position:'absolute'
                                                }}}
                                        />
                                    </Item>
                                </View>
                            </View>
                            <View style={{marginTop:15,paddingRight:10,paddingLeft:10}}>
                                <Item picker style={{textAlign:'right',borderBottomWidth:0}}>
                                    <CustomPicker
                                        value={this.state.cat_parent?this.state.categories.filter((cat)=>{if(cat.id == this.state.cat_parent) return true})[0]:null}
                                        placeholder={'انتخاب دسته بندی'}
                                        options={this.state.categories}
                                        getLabel={item => item.title}
                                        containerStyle={filter.smallPicker}
                                        onValueChange={value=>value!=null?this.getSubCat(value.id):this.getSubCat(0)}
                                        fieldTemplateProps={{
                                            containerStyle: {
                                                borderBottomColor: '#fff',
                                                textAlign:'right',
                                                borderBottomWidth:0,
                                            },
                                            textStyle: {
                                                color: '#454545',
                                                fontSize:14,
                                                textAlign:'right',
                                                right:0,
                                                position:'absolute',
                                                fontFamily:'IRANYekanMobileFN'
                                            },
                                        }}
                                        optionTemplateProps={{
                                            containerStyle: {
                                                backgroundColor: '#fff',
                                                height: 60,
                                                borderBottomWidth:0,
                                            },
                                            textStyle: {
                                                color: '#343434',
                                                right:15,
                                                fontFamily:'IRANYekanMobileFN',
                                                position:'absolute'
                                            }}}
                                    />
                                </Item>
                                {
                                    this.state.subCatLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                                }
                                {
                                    this.state.subCatPicker
                                }

                                {
                                    this.state.itemLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                                }

                                {
                                    this.state.categoryItems.map((item)=>{
                                        if(item.type == 'select'){
                                            return(
                                                <Item key={item.title} picker style={{textAlign:'right',borderBottomWidth:0,marginTop:15}}>
                                                    <CustomPicker
                                                        placeholder={item.title}
                                                        options={JSON.parse(item.data)}
                                                        getLabel={item => item}
                                                        containerStyle={filter.smallPicker}
                                                        onValueChange={value=>value!=null?this.state.search.datas[item.id]= value:this.state.search.datas[item.id]=''}
                                                        fieldTemplateProps={{
                                                            containerStyle: {
                                                                borderBottomColor: '#fff',
                                                                textAlign:'right',
                                                                borderBottomWidth:0,
                                                            },
                                                            textStyle: {
                                                                color: '#454545',
                                                                fontSize:14,
                                                                textAlign:'right',
                                                                right:0,
                                                                position:'absolute',
                                                                fontFamily:'IRANYekanMobileFN'
                                                            },
                                                        }}
                                                        optionTemplateProps={{
                                                            containerStyle: {
                                                                backgroundColor: '#fff',
                                                                height: 60,
                                                                borderBottomWidth:0,
                                                            },
                                                            textStyle: {
                                                                color: '#343434',
                                                                right:15,
                                                                fontFamily:'IRANYekanMobileFN',
                                                                position:'absolute'
                                                            }}}
                                                    />
                                                </Item>
                                            )
                                        }
                                        if(item.type == 'switch'){
                                            let index = this.state.switch.indexOf(this.state.switch.filter((sw)=>{if(sw.id == item.id){return true}})[0])
                                            return(
                                                <Item key={item.id} style={{flexDirection:'row-reverse',justifyContent:'flex-start',paddingTop:10,paddingBottom:10}}>
                                                    <Switch
                                                        value={this.state.switch[index].value}
                                                        onValueChange={() => {this.state.switch[index].value=!this.state.switch[index].value;this.state.search.datas[item.id]=this.state.switch[index].value?'0':'1';this.forceUpdate();}}
                                                    />
                                                    <Text style={createAd.switchText}>{item.title}</Text>
                                                </Item>
                                            )
                                        }
                                    })
                                }
                            </View>
                            </ScrollView>
                            <View style={{marginTop:50}}>
                                <Button style={filter.Button} full onPress={()=>this.setFilter()}>
                                    <Text style={filter.ButtonText}>اعمال فیلتر</Text>
                                </Button>
                            </View>
                        </View>
                    </View>:null
                }
                {
                    this.state.exit?
                        <View style={filter.lightBox}>
                            <View style={filter.lightBoxContent}>
                                <Text style={filter.exitText}>برای خروج از اپلیکیشن دوباره روی دکمه بازگشت کلیک کنید</Text>
                            </View>
                        </View>
                        :null
                }
                <Content style={{flex:1, marginTop: 40}}  contentContainerStyle={{flex: 1}}>
                    <View style={list.searchParent}>
                        <Item regular style={list.searchItem}>
                            <Input placeholder='جستجو در آگهی ها' style={list.searchInput} onChangeText={(val)=>{this.setState({searchInput:val})}} returnKeyType='search' onSubmitEditing={this.submitSrearch.bind(this)} />
                            <Icon name={'md-search'} style={{position:'absolute',left:0}} onPress={this.submitSrearch.bind(this)} />
                            <Button style={list.filterButton} onPress={()=>{this.setState({lightBox:true});this.forceUpdate()}}><Icon style={list.filterButtonIcon}  type="FontAwesome" name="filter" /><Text style={list.filterButtonText}>فیلتر کنید</Text></Button>
                        </Item>
                    </View>
                    <FlatList
                        onScroll={this.onScroll.bind(this)}
                        ListHeaderComponent={this.renderHeader.bind(this)}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        data={this.state.advertisements}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={0.01}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                    />
                    {
                        this.state.showSuggestion?
                            <Animated.View style={[list.suggestion,{transform:[{ translateY: suggest }]}]}><TouchableOpacity onPress={()=>Actions.shopList({id:this.state.filter.categoryId})}><Text style={list.suggestionText}>جستجو میان فروشندگان</Text></TouchableOpacity><TouchableOpacity onPress={()=>this.setState({showSuggestion:false})} style={{position:'absolute',left:5,top:4}}><Icon name='md-close-circle' style={{ fontSize : 30 , color : '#fff'}}/></TouchableOpacity></Animated.View>:null
                    }
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[list.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>{if(this.state.login){Actions.myFabrika()}else{Actions.loginCheck()}}}>
                            <Icon style={list.footerIcon} name="md-person" />
                            <Text style={list.footerText}>فابریکای من</Text>
                        </Button>
                        <Button style={[list.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>{if(this.state.login){Actions.myAds()}else{Actions.loginCheck()}}}>
                            <Icon style={list.footerIcon} name="md-add" />
                            <Text style={list.footerText}>آگهی های من</Text>
                        </Button>
                        <Button style={list.pageFooterButton} onPress={()=>Actions.advanceSearch()}>
                            <Icon style={list.footerIcon} name="md-search" />
                            <Text style={list.footerText}>جستجو پیشرفته</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    getAdRequest() {
        const { page } = this.state;
        if(!this.state.adLoading){
            this.setState({
                adLoading:true,
                noAd:null
            })
            pexios('/advertisement/show','post',
                {page:page,
                    cat_parent:this.state.cat_parent?this.state.cat_parent:null,
                    category_id:this.state.filter.categoryId?this.state.filter.categoryId:null,
                    priority:this.state.filter.priority?this.state.filter.priority:null,
                    pricemin:this.state.filter.pricemin?this.state.filter.pricemin:null,
                    pricemax:this.state.filter.pricemax?this.state.filter.pricemax:null,
                    orderby:this.state.filter.orderby?this.state.filter.orderby:'idDesc',
                    city_id:this.state.search.city_id?this.state.search.city_id:null,
                    state_id:this.state.search.state_id?this.state.search.state_id:null,
                    q:this.state.search.searchKey?this.state.search.searchKey:null,
                    filter:this.state.search.datas?Object.keys(this.state.search.datas).length === 0 && this.state.search.datas.constructor === Object?null:JSON.stringify(this.state.search.datas):null
                }).then(res=>{
                let adv = res.data.data.data;
                    this.setState(prevState=>{
                        return {
                            advertisements: page===1?adv:[...prevState.advertisements, ...adv],
                            page: res.data.data.current_page + 1,
                            lastPage: res.data.data.last_page,
                            refreshing : false
                        }
                    })
                if(!this.state.advertisements.length){
                        this.setState({
                            noAd:'نتیجه ای یافت نشد، لطفا عناوین جستجو یا مقادیر فیلتر را تغییر دهید'
                        })
                }
                this.setState({adLoading:false})
            })
        }
    }
    renderItem({ item }) {
        return <Advertisement ad={item} myAd={false}/>
    }
    handleLoadMore(){
        if(this.state.advertisements.length > 0 && this.state.page <= this.state.lastPage) {
            this.getAdRequest()
        }
    }
    handleRefresh(){
        this.setState({ page : 1 , refreshing : true } , () => {
            this.getAdRequest();
        })
    }
    renderHeader(){
        if(!this.state.categories.length) return null;
        return (
            <View>
                <View style={list.catParent}>
                    {
                        this.state.categories.map((cat) =>{
                            return <View style={list.catChild} key={cat.id}><TouchableOpacity onPress={this.catFilter.bind(this,cat.id)} style={this.state.cat_parent != cat.id?list.category:list.categorySelected}>{cat.attachments?<Image source={{uri :url+'/files?uid='+cat.attachments.uid+'&width=27&height=27'}} style={this.state.cat_parent != cat.id?list.categoryImage:list.categoryImageSelected} />:<Image source={require('../assets/images/no-image.png')} style={list.categoryImage} />}<Text  style={list.categoryText}>{cat.title}</Text></TouchableOpacity></View>
                        })
                    }
                    <View style={list.catChild}><TouchableOpacity onPress={()=>{if(this.state.login){Actions.tenderList()}else{Actions.loginCheck()}}}  style={list.category}><Image source={require('../assets/images/auction.png')} style={list.categoryImage} /><Text style={list.categoryText}>مناقصات</Text></TouchableOpacity></View>
                    <View style={list.catChild}><TouchableOpacity disabled={this.state.withoutFilter} onPress={this.removeFilter.bind(this)} style={[list.category,{opacity:this.state.withoutFilter?.5:1}]}><Image source={require('../assets/images/delete.png')} style={list.categoryImage} /><Text style={list.categoryText}>حذف فیلتر</Text></TouchableOpacity></View>
                </View>
                {
                    this.state.noAd?
                        <Text style={list.noAd}>{this.state.noAd}</Text>:null
                }
            </View>

        )
    }
    renderFooter(){
        if(!this.state.adLoading) return null;

        return <View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
    }
    catFilter(id){
        pexios('/advertisement/sub-category/'+id,'get').then(res=>{
            this.setState({
                subCategories:res.data,
                cat_parent:id,
                page:1,
                advertisements:[],
                withoutFilter:false,
                subCatPicker:null,
                categoryItems:[],
                filter:{
                    pricemin:null,
                    pricemax:null,
                    orderby:null,
                    priority:null,
                    categoryId:null,
                },
                search:{
                    state_id:null,
                    city_id:null,
                    datas:{},
                    searchKey:null
                },
            },()=>{
                this.getAdRequest()
            })
        })
    }
    setFilter(){
        this.getSubCat(this.state.cat_parent)
        AsyncStorage.setItem('filter',JSON.stringify({filter:this.state.filter,search:this.state.search,cat_parent:this.state.cat_parent}))
        this.setState({
            lightBox:false,
            page:1,
            advertisements:[],
            withoutFilter:false,
        },()=>{
            this.getAdRequest()
        })
    }
    submitSrearch(){
        this.setState(prevState=>{
            return{
                page:1,
                advertisements:[],
                withoutFilter:false,
                search:{
                    ...prevState.search,
                    searchKey:this.state.searchInput,
                },
            }
        },()=>{
            this.getAdRequest()
        })
    }
    removeFilter(){
        AsyncStorage.removeItem('filter')
        this.setState({
            page:1,
            advertisements:[],
            subCategories:null,
            cat_parent:null,
            withoutFilter:true,
            subCatPicker:null,
            categoryItems:[],
            filter:{
                pricemin:null,
                pricemax:null,
                orderby:null,
                priority:null,
                categoryId:null,
            },
            search:{
                state_id:null,
                city_id:null,
                datas:{},
                searchKey:null
            },
        },()=>{
            this.getAdRequest()
        })
    }
    onScroll(event){
        let currentOffset = event.nativeEvent.contentOffset.y;
        let direction = currentOffset > this.offset ? 'down' : 'up';
        this.offset = currentOffset
        if(!this.state.showSuggestion){
            if(this.state.advertisements.length >= 15 ){
                this.setState({
                    showSuggestion:true
                })
            }
        }
        if(this.state.showSuggestion && direction=='up' && this.animateTime){
            this.animateTime = false
            Animated.timing(this.suggestValue,{
                toValue:0,
                duration:500,
            }).start()
            setTimeout(()=>{
                this.animateTime = true
            },600)
        }
        if(this.state.showSuggestion && direction=='down' && this.animateTime){
            this.animateTime = false
            Animated.timing(this.suggestValue,{
                toValue:1,
                duration:500,
            }).start()
            setTimeout(()=>{
                this.animateTime = true
            },600)
        }
    }
    getSubCat(id){
        if(id==0){
            this.setState({subCatPicker:null})
        }else {
            this.setState({subCatPicker: null, subCatLoader: true,cat_parent:id})
            pexios('/advertisement/sub-category/' + id, 'get').then(res => {
                this.setState({
                    subCategories: res.data,
                }, () => {
                    this.setState({
                        subCatLoader: false,
                        subCatPicker: <Item picker style={{textAlign: 'right',borderBottomWidth:0,marginTop:15}}>
                            <CustomPicker
                                value={this.state.filter.categoryId?this.state.subCategories.filter((cat)=>{if(cat.id == this.state.filter.categoryId) return true})[0]:null}
                                placeholder={'انتخاب زیر دسته '}
                                options={this.state.subCategories}
                                getLabel={item => item.title}
                                containerStyle={filter.smallPicker}
                                onValueChange={value => value != null ? this.getField(value.id) : 0}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign: 'right',
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize: 14,
                                        textAlign: 'right',
                                        right: 0,
                                        position: 'absolute',
                                        fontFamily: 'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth: 0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right: 15,
                                        fontFamily: 'IRANYekanMobileFN',
                                        position: 'absolute'
                                    }
                                }}
                            />
                        </Item>
                    })
                })
            })
        }
    }
    getField(id){
        this.setState(prevState=>{
            return{
                itemLoader:true,
                filter:{
                    ...prevState.filter,
                    categoryId:id
                }
            }
        })
        pexios('/advertisement/data-category/'+id,'get').then(res=>{
            let catData = res.data
            res.data.items.filter((sw)=>{if(sw.type=='switch'){
                this.setState(prevState=> {
                    return {
                        switch: [...prevState.switch, {id: sw.id, value: sw.value==1?true:false}],
                    }
                })
                this.state.search.datas[sw.id]= sw.value
            }})
            this.setState({
                categoryItems:res.data.items,
                itemLoader:false,
            })

        })
    }
}