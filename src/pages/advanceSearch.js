import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    Spinner,
    Textarea,
    CheckBox
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { createAd } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {BackHandler, Image, Switch} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

export default class advanceSearch extends Component{
    constructor(props){
        super(props)
        this.state={
            states:[],
            city:[],
            categories:[],
            subCategories:[],
            cityPicker:null,
            subCatPicker:null,
            subCatLoader:false,
            categoryItems:[],
            itemLoader:false,
            datas:{},
            disableBtn:false,
            switch:[],
            selectedState:null,
            selectedCity:null,
            selectedCat:null,
            selectedSubCat:null,
            searchKey:null,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/advertisement/data-states','get').then(res=>{
            this.setState({
                states:res.data
            })
        })
        pexios('/advertisement/category/list','get').then(res=>{
            this.setState({
                categories:res.data.data,
            })
        })
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={createAd.container}>
                    <View style={{flex:1}}>
                        <Item regular style={createAd.inputCode}><Input style={createAd.inputCodeChild} placeholder='جستجو در آگهی ها' onChangeText={this.changeSearch.bind(this)} /></Item>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب استان'}
                                options={this.state.states}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.getCity(value.id):this.getCity(0)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#343434',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#fff',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        {
                            this.state.cityLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        {
                            this.state.cityPicker
                        }
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب دسته بندی'}
                                options={this.state.categories}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.getSubCat(value.id):this.getSubCat(0)}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>

                        {
                            this.state.subCatLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        {
                            this.state.subCatPicker
                        }

                        {
                            this.state.itemLoader?<View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>:null
                        }
                        {
                            this.state.categoryItems.map((item)=>{
                                if(item.type=='text'){
                                    return <Item key={item.id} regular style={createAd.inputCode}><Input style={createAd.inputCodeChild} placeholder={item.title} onChangeText={this.changeText.bind(this,item.id)} /></Item>
                                }
                                if(item.type == 'select'){
                                    return(
                                        <Item key={item.title} picker style={{textAlign:'right'}}>
                                            <CustomPicker
                                                placeholder={item.title}
                                                options={JSON.parse(item.data)}
                                                getLabel={item => item}
                                                containerStyle={createAd.picker}
                                                onValueChange={value=>value!=null?this.state.datas[item.id]= value:this.state.datas[item.id]=''}
                                                fieldTemplateProps={{
                                                    containerStyle: {
                                                        borderBottomColor: '#fff',
                                                        textAlign:'right'
                                                    },
                                                    textStyle: {
                                                        color: '#454545',
                                                        fontSize:18,
                                                        textAlign:'right',
                                                        right:0,
                                                        position:'absolute',
                                                        fontFamily:'IRANYekanMobileFN'
                                                    },
                                                }}
                                                optionTemplateProps={{
                                                    containerStyle: {
                                                        backgroundColor: '#343434',
                                                        height: 60,
                                                        borderBottomWidth:0,
                                                    },
                                                    textStyle: {
                                                        color: '#fff',
                                                        right:15,
                                                        fontFamily:'IRANYekanMobileFN',
                                                        position:'absolute'
                                                    }}}
                                            />
                                        </Item>
                                    )
                                }
                                if(item.type == 'switch'){
                                    let index = this.state.switch.indexOf(this.state.switch.filter((sw)=>{if(sw.id == item.id){return true}})[0])
                                    return(
                                        <Item key={item.id} style={{flexDirection:'row-reverse',justifyContent:'flex-start',paddingTop:10,paddingBottom:10}}>
                                            <Switch
                                                value={this.state.switch[index].value}
                                                onValueChange={() => {this.state.switch[index].value=!this.state.switch[index].value;this.state.datas[item.id]=this.state.switch[index].value;this.forceUpdate();}}
                                            />
                                            <Text style={createAd.switchText}>{item.title}</Text>
                                        </Item>
                                    )
                                }
                                if(item.type == 'text-box'){
                                    return(
                                        <Item key={item.id} regular style={createAd.inputCode}>
                                            <Textarea rowSpan={5} bordered placeholder={item.title} style={createAd.textArea}  onChangeText={this.changeText.bind(this,item.id)} />
                                        </Item>
                                    )
                                }
                            })
                        }

                        <Button style={createAd.Button} full disabled={this.state.disableBtn} onPress={()=>this.submitSearch()}>
                            <Text style={createAd.ButtonText}>جستجو</Text>
                        </Button>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
    changeSearch(value){
        this.setState({
            searchKey:value
        })
    }
    changeText(id,value){
        this.state.datas[id]=value
    }
    selectCity(city_id){
        this.setState({
            selectedCity:city_id,
        })
    }
    getCity(id){
        if(id==0){
            this.setState({cityPicker:null})
        }else {
            this.setState({cityPicker: null, cityLoader: true,selectedState:id})
            pexios('/advertisement/data-cities/' + id, 'get').then(res => {
                this.setState({
                    city: res.data,
                }, () => {
                    this.setState({
                        cityLoader: false,
                        cityPicker: <Item picker style={{textAlign: 'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب شهر'}
                                options={this.state.city}
                                getLabel={item => item.title}
                                onValueChange={value => {
                                    value != null ? this.selectCity(value.city_id) : 0
                                }}
                                containerStyle={createAd.picker}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign: 'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize: 18,
                                        textAlign: 'right',
                                        right: 0,
                                        position: 'absolute',
                                        fontFamily: 'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#343434',
                                        height: 60,
                                        borderBottomWidth: 0,
                                    },
                                    textStyle: {
                                        color: '#fff',
                                        right: 15,
                                        fontFamily: 'IRANYekanMobileFN',
                                        position: 'absolute'
                                    }
                                }}
                            />
                        </Item>
                    })
                })

            })
        }
    }
    getSubCat(id){
        if(id==0){
            this.setState({subCatPicker:null})
        }else {
            this.setState({subCatPicker: null, subCatLoader: true,selectedCat:id})
            pexios('/advertisement/sub-category/' + id, 'get').then(res => {
                this.setState({
                    subCategories: res.data,
                }, () => {
                    this.setState({
                        subCatLoader: false,
                        subCatPicker: <Item picker style={{textAlign: 'right'}}>
                            <CustomPicker
                                placeholder={'انتخاب زیر دسته '}
                                options={this.state.subCategories}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value => value != null ? this.getField(value.id) : 0}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign: 'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize: 18,
                                        textAlign: 'right',
                                        right: 0,
                                        position: 'absolute',
                                        fontFamily: 'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#343434',
                                        height: 60,
                                        borderBottomWidth: 0,
                                    },
                                    textStyle: {
                                        color: '#fff',
                                        right: 15,
                                        fontFamily: 'IRANYekanMobileFN',
                                        position: 'absolute'
                                    }
                                }}
                            />
                        </Item>
                    })
                })
            })
        }
    }
    getField(id){
        this.setState({itemLoader:true,selectedSubCat:id})
        pexios('/advertisement/data-category/'+id,'get').then(res=>{
            console.log(res.data)
            let catData = res.data
            res.data.items.filter((sw)=>{if(sw.type=='switch'){
                this.setState(prevState=> {
                    return {
                        switch: [...prevState.switch, {id: sw.id, value: sw.value==1?true:false}],
                    }
                })
                this.state.datas[sw.id]= sw.value
            }})
            this.setState({
                categoryItems:res.data.items,
                itemLoader:false,
            })

        })
    }
    submitSearch(){
        Actions.replace('list',{
            search:{
                datas:this.state.datas,
                state_id:this.state.selectedState,
                city_id:this.state.selectedCity,
                selectedCat:this.state.selectedCat,
                selectedSubCat:this.state.selectedSubCat,
                searchKey:this.state.searchKey
            }
        })
    }
}