import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Icon,
    Input,
    Form,
    Spinner,
    Textarea,
    CheckBox, Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { createAd } from './../assets/styles';
import { CustomPicker } from 'react-native-custom-picker'
import {Alert, BackHandler, Image, Switch} from "react-native";
import ImagePicker from 'react-native-image-crop-picker';
import {object2formData,pexios,url} from '../component/helper'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

export default class CreateAd extends Component{
    constructor(props){
        super(props)
        this.state={
            credit:[
                {
                    title:'طلایی',
                    value:'gold'
                },
                {
                    title:'نقره ای',
                    value:'silver'
                },
            ],
            images:[
                {uri : url+'/images/add_image.png',changed:false},
                {uri : url+'/images/add_image.png',changed:false},
                {uri : url+'/images/add_image.png',changed:false}
            ],
            selectedCredit:null
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <KeyboardAwareScrollView style={{backgroundColor:'#f4f4f4'}}>
                <View style={createAd.container}>
                    <Content style={{flex:1}}>
                        <Item picker style={{textAlign:'right'}}>
                            <CustomPicker
                                placeholder={'اعتبار'}
                                options={this.state.credit}
                                getLabel={item => item.title}
                                containerStyle={createAd.picker}
                                onValueChange={value=>value!=null?this.setState({selectedCredit:value.value}):this.setState({selectedCredit:null})}
                                fieldTemplateProps={{
                                    containerStyle: {
                                        borderBottomColor: '#fff',
                                        textAlign:'right'
                                    },
                                    textStyle: {
                                        color: '#454545',
                                        fontSize:18,
                                        textAlign:'right',
                                        right:0,
                                        position:'absolute',
                                        fontFamily:'IRANYekanMobileFN'
                                    },
                                }}
                                optionTemplateProps={{
                                    containerStyle: {
                                        backgroundColor: '#fff',
                                        height: 60,
                                        borderBottomWidth:0,
                                    },
                                    textStyle: {
                                        color: '#343434',
                                        right:15,
                                        fontFamily:'IRANYekanMobileFN',
                                        position:'absolute'
                                    }}}
                            />
                        </Item>
                        <View style={createAd.imgParent}>
                            {
                                this.state.images.map((img,index)=>{
                                    return <Button onPress={this.changeImage.bind(this,index)} key={index} transparent style={{width:100,height:100,marginTop:10}}><Image source={img} style={{width:100,height:100}} /></Button>
                                })
                            }
                        </View>
                        <Button style={createAd.Button} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={createAd.ButtonText}>ایجاد آگهی</Text>
                            }
                        </Button>
                    </Content>
                </View>
            </KeyboardAwareScrollView>
        )
    }

    changeImage(index,img){
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true
        }).then(image => {
            this.state.images[index]={uri:image.path,changed:true}
            this.forceUpdate()
        });
    }
    async submitForm(){
        this.setState({disableBtn:true})
        let fd = object2formData({
            title:this.state.selectedCredit,
            data:JSON.stringify({shop_id:this.props.id})
        })
        this.state.images.map((img)=>{
            if(img.changed){
                fd.append("documents[]",{uri:img.uri,type:"image/jpg",name:"image[]"});
            }
        })
        console.log(fd)
        pexios('/financial/create_invoice','post',fd,true).then(res=>{
            Alert.alert(
                'ارور',
                'نیازمند درگاه پرداخت',
            );
            this.setState({disableBtn:false})
        }).catch(err=>{
            console.log(err)
            this.setState({disableBtn:false})
        })
    }
}