import React, {Component} from 'react';
import { Container , Header , View , Text , Left , Button , Right , Content , Form , Item , Icon , Input } from 'native-base';
import {splashSC} from "../assets/styles";
import {AsyncStorage, BackHandler, Image, StatusBar, Dimensions, Animated, Easing} from "react-native";
import { Actions } from 'react-native-router-flux';
import VerticalViewPager from 'react-native-vertical-view-pager';
const { width, height } = Dimensions.get('window');
export default class help extends Component {
    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
    }
        componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.animate();
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    animate() {
        Animated.timing(this.animatedValue , {
            toValue : 1,
            duration : 1000,
        }).start(() => this.animateBack())
    }
    animateBack() {
        Animated.timing(this.animatedValue , {
            toValue : 0,
            duration : 1000,
        }).start(() => this.animate())
    }
    render() {
        const translate = this.animatedValue.interpolate({
            inputRange : [0,1],
            outputRange : [0 , -10]
        });
        return (
            <Container style={splashSC.containerSlider}>
                <StatusBar backgroundColor="transparent" translucent barStyle="light-content"/>
                <VerticalViewPager showsVerticalScrollIndicator={false}>
                    <View style={[splashSC.slide,{width,height}]}>
                        <Image style={splashSC.image} source={require('../assets/images/single.png')} />
                        <View style={splashSC.descParent}>
                            <Text style={[splashSC.descTitle,splashSC.bold]}>با اطمینان و به قیمت خرید کنید</Text>
                            <Text style={splashSC.desc}>
                                وارد برنامه فابریکا شوید و با استفاده از دکمه های فیلتر و جستجوی پیشرفته، اجناس مورد نیاز خود را پیدا کنید.
                            </Text>
                            <Animated.View style={{position:'absolute',bottom:5,transform:[{ translateY: translate }]}}><Icon name='chevron-down' type="FontAwesome" /></Animated.View>
                        </View>
                    </View>
                    <View style={[splashSC.slide,{width,height}]}>
                        <Image style={splashSC.image} source={require('../assets/images/single.png')} />
                        <View style={splashSC.descParent}>
                            <Text style={[splashSC.descTitle,splashSC.bold]}>سریع و راحت سود کنید</Text>
                            <Text style={splashSC.desc}>
                                با فابریکا، رایگان و بدون هزینه کالای خود را آگهی کنید و اجناس خود را سریع و راحت بفروشید.
                            </Text>
                            <Animated.View style={{position:'absolute',bottom:5,transform:[{ translateY: translate }]}}><Icon name='chevron-down' type="FontAwesome" /></Animated.View>
                        </View>
                    </View>
                    <View style={[splashSC.slide,{width,height}]}>
                        <Image style={splashSC.image} source={require('../assets/images/single.png')} />
                        <View style={splashSC.descParent}>
                            <Text style={[splashSC.descTitle,splashSC.bold]}>فروشگاه ایجاد کنید و کسب و کار خود را توسعه دهید</Text>
                            <Text style={splashSC.desc}>
                                با ایجاد فروشگاه و درخواست اعتبار از فابریکا، مشتریان بیشتری جذب کنید و اجناس فروشگاه خود را سریعتر از دیگران بفروشید.
                            </Text>
                            <Animated.View style={{position:'absolute',bottom:5,transform:[{ translateY: translate }]}}><Icon name='chevron-down' type="FontAwesome" /></Animated.View>
                        </View>
                    </View>
                    <View style={[splashSC.slide,{width,height}]}>
                        <Image style={splashSC.image} source={require('../assets/images/single.png')} />
                        <View style={splashSC.descParent}>
                            <Text style={[splashSC.descTitle,splashSC.bold]}>اگر خبره و استادکار کالاهای صنعتی هستید، برای خود درآمد ایجاد کنید.</Text>
                            <Text style={splashSC.desc}>
                                خریداران، به نظرات کارشناسی شما نیاز دارند، اگر کارشناس و استادکار هستید، در فابریکا ثبت نام کنید و برای خود درآمد ایجاد نمایید.
                            </Text>
                            <Animated.View style={{position:'absolute',bottom:5,transform:[{ translateY: translate }]}}><Icon name='chevron-down' type="FontAwesome" /></Animated.View>
                        </View>
                    </View>
                    <View style={[splashSC.slide,{width,height}]}>
                        <Image style={splashSC.image} source={require('../assets/images/single.png')} />
                        <View style={splashSC.buttonParent}>
                            <Button full style={splashSC.button} onPress={this.signin.bind(this)}>
                                <Text style={splashSC.buttonText}>ثبت رایگان آگهی</Text>
                            </Button>
                            <Button full style={[splashSC.button,{marginTop:10}]} onPress={this.createExpert.bind(this)}>
                                <Text style={splashSC.buttonText}>ثبت نام کارشناس خبره</Text>
                            </Button>
                            <Button full style={[splashSC.button,{marginTop:10}]} onPress={this.createShop.bind(this)}>
                                <Text style={splashSC.buttonText}>ایجاد فروشگاه</Text>
                            </Button>
                            <Button full style={[splashSC.button,{marginTop:10}]} onPress={this.withoutSignIn.bind(this)}>
                                <Text style={splashSC.buttonText}>ورود</Text>
                            </Button>
                        </View>
                    </View>
                </VerticalViewPager>
            </Container>
        )
    }
    signin(){
        AsyncStorage.setItem('opened' , 'opened' );
        Actions.replace('loginCheck')
    }
    createShop(){
        AsyncStorage.setItem('opened' , 'opened' );
        Actions.replace('loginCheck',{shop:true})
    }
    createExpert(){
        AsyncStorage.setItem('opened' , 'opened' );
        Actions.replace('loginCheck',{expert:true})
    }
    withoutSignIn(){
        AsyncStorage.setItem('opened' , 'opened' );
        Actions.replace('list')
    }
}