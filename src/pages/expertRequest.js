import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab,
    Spinner,
    Badge, Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Fabrika } from './../assets/styles';
import { pexios, url} from '../component/helper'
import {BackHandler, Image, TouchableOpacity} from "react-native";
import {list, shopL, single} from "../assets/styles";
import myFabrika from "./myFabrika";
export default class expertRequest extends Component{
    constructor(props) {
        super(props)
        this.state = {
            request:[],
            history:[],
            loaderRequest:false,
            loaderHistory:false,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        pexios('/expert/wait-list','get').then(res=>{
            this.setState({
                request:res.data.data,
                loaderRequest:true
            },()=>console.log(this.state.request))
        })
        pexios('/expert/accepted-list','get').then(res=>{
            this.setState({
                history:res.data.data,
                loaderHistory:true
            },()=>console.log(this.state.history))
        })

    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={Fabrika.container}>
                <Content style={{flex:1}}>
                    <View>
                        <Text style={Fabrika.expertTitle}>درخواست های در انتظار تایید:</Text>
                        {
                            this.state.loaderRequest?
                            this.state.request.length?
                            this.state.request.map((usr,index)=>{
                                return(
                                    <Item style={Fabrika.expertParent} key={usr.id}>
                                        <Image source={{uri:usr.user.attachments?url+'/files?uid='+usr.user.attachments.uid+'&width=64&height=64':url+'/images/profile.png'}} style={Fabrika.expertUserPix} />
                                        <Text style={Fabrika.expertUserText}>{usr.user.fname} {usr.user.lname}</Text>
                                        <Text style={Fabrika.expertUserText}>{usr.user.mobile}</Text>
                                        <View>
                                            <Badge style={{width:100}} danger><Text onPress={this.rejectRequest.bind(this,usr.id,index)} style={Fabrika.btnText}> رد درخواست </Text></Badge>
                                            <Badge style={{width:100,marginTop:5}} success><Text onPress={this.acceptRequest.bind(this,usr.id,index)} style={Fabrika.btnText}> تایید </Text></Badge>
                                            <Badge style={{width:100,marginTop:5}} info><Text onPress={()=>Actions.single({ad:usr.advertisement,myAd:false})} style={Fabrika.btnText}> مشاهده </Text></Badge>
                                        </View>
                                    </Item>
                                )
                            }):<Text style={Fabrika.expertTitle}>موردی برای نمایش وجود ندارد</Text>:<View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
                        }

                    </View>
                    <View>
                        <Text style={Fabrika.expertTitle}>درخواست های تایید شده:</Text>
                        {

                            this.state.loaderHistory?
                            this.state.history.length?
                            this.state.history.map((usr,index)=>{
                                return(
                                    <Item style={Fabrika.expertParent} key={usr.id}>
                                        <Image source={{uri:usr.user.attachments?url+'/files?uid='+usr.user.attachments.uid+'&width=64&height=64':url+'/images/profile.png'}} style={Fabrika.expertUserPix} />
                                        <Text style={Fabrika.expertUserText}>{usr.user.fname} {usr.user.lname}</Text>
                                        <Text style={Fabrika.expertUserText}>{usr.user.mobile}</Text>
                                        <View>
                                            {
                                                usr.done===0?<Badge style={{width:100}} success><Text style={Fabrika.btnText} onPress={this.doneRequest.bind(this,usr.id,index)}> انجام شد </Text></Badge>:null
                                            }
                                            <Badge style={usr.done===0?{width:100,marginTop:5}:{width:100}} info><Text  onPress={()=>Actions.single({ad:usr.advertisement,myAd:false})} style={Fabrika.btnText}> مشاهده </Text></Badge>
                                        </View>
                                    </Item>
                                )
                            }):<Text style={Fabrika.expertTitle}>موردی برای نمایش وجود ندارد</Text>:<View style={list.loaderParent}><Spinner color={'#f2c200'} /></View>
                        }

                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.myAds()}>
                            <Icon style={shopL.footerIcon} name="md-add" />
                            <Text style={shopL.footerText}>آگهی های من</Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>Actions.replace('list')}>
                            <Icon style={shopL.footerIcon} name="md-home" />
                            <Text style={shopL.footerText}>صفحه اصلی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    acceptRequest(id,index){
        pexios('/expert/edit-list','post',{id:id,status:1}).then(res=>{
            Toast.show({
                text: 'درخواست شما با موفقیت ثبت شد',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'success'
            })
            this.state.request.splice(index,1)
            this.state.history.push(res.data.data)
            this.forceUpdate()
        })
    }
    rejectRequest(id,index){
        pexios('/expert/edit-list','post',{id:id,remove:true}).then(res=>{
            Toast.show({
                text: 'درخواست شما با موفقیت ثبت شد',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'success'
            })
            this.state.request.splice(index,1)
            this.forceUpdate()
        })
    }
    doneRequest(id,index){
        pexios('/expert/edit-list','post',{id:id,done:1}).then(res=>{
            Toast.show({
                text: 'درخواست شما با موفقیت ثبت شد',
                textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                duration: 3000,
                type:'success'
            })
        })
        this.state.history[index].done=1
        this.forceUpdate()
    }
}