import React, {Component} from 'react';
import {
    View,
    Text,
    Container,
    Button,
    Content,
    Item,
    Input,
    Form,
    Icon,
    Footer,
    FooterTab,
    Spinner,
    Body, CheckBox, Textarea, Toast
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Fabrika } from './../assets/styles';
import { pexios, url} from '../component/helper'
import {BackHandler, TouchableOpacity} from "react-native";
import {createAd, shopL, signup, single} from "../assets/styles";
import {CustomPicker} from "react-native-custom-picker";
export default class editExpert extends Component{
    constructor(props) {
        super(props)
        this.state = {
            categories:[],
            checkBox:[],
            selectedCat:[],
            loader:true,
            disableBtn:false,
            description:null,
            catLoader:true,
            requiredFields:[],
            national_id:null,
            birthday:null,
            history:null,
            his:[
                '1 سال',
                '2 سال',
                '3 سال',
                '4 سال',
                '5 سال',
                '6 سال',
                '7 سال',
                '8 سال',
                '9 سال',
                '10 سال',
                '11 سال',
                '12 سال',
                '13 سال',
                '14 سال',
                '15 سال',
                '16 سال',
                '17 سال',
                '18 سال',
                '19 سال',
                '20 سال',
            ],
            year:null,
            month:null,
            day:null,
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.setState({description:this.props.desc,national_id:this.props.NI,history:this.props.history,year:this.props.birthday.split('-')[0],month:this.props.birthday.split('-')[1],day:this.props.birthday.split('-')[2]})
        if(this.props.cats){
            pexios('/advertisement/category/list','get').then(res=>{
                this.setState({
                    categories:res.data.data,
                    catLoader:false,
                },async ()=>{
                    await this.state.categories.map((cat)=>{
                        cat.subs.map((sub)=>{
                            this.setState(prevState=> {
                                return {
                                    checkBox: [...prevState.checkBox, {id:sub.id,value:false}],
                                }
                            })
                        })
                    })
                    this.props.cats.map(prop=>{
                        this.state.checkBox.map((cb,index)=>{
                            if(cb.id == prop.id){
                                this.state.checkBox[index]={id:cb.id,value:true}
                                this.state.selectedCat.push(cb.id)
                                this.forceUpdate()
                            }
                        })
                    })
                })
            })
        }else{
            pexios('/advertisement/category/list','get').then(res=>{
                this.setState({
                    categories:res.data.data,
                    catLoader:false,
                },()=>{
                     this.state.categories.map((cat)=>{
                        cat.subs.map((sub)=>{
                            this.setState(prevState=> {
                                return {
                                    checkBox: [...prevState.checkBox, {id:sub.id,value:false}],
                                }
                            })
                        })
                    })
                })
            })
        }
    }
    handleBackButton(){
        Actions.pop()
        return true;
    }
    render(){
        return(
            <Container style={Fabrika.container}>
                <Content style={{flex:1}}>
                    <View style={{paddingLeft:20,paddingRight:20}}>
                        <View style={{backgroundColor:'white',padding:10,marginTop:10}}>
                            <Text style={signup.description}>
                                با توجه به تخصصی بودن خرید و فروش کالاهای فابریک و استوک صنعتی، بسیاری از مشتریان نیازمند مشاوره در خریدهای خود هستند. اگر استادکار و کارشناسید، به خریداران مشاوره دهید و برای خود درآمد ایجاد کنید.
                            </Text>
                        </View>
                        <Text style={signup.expertTitle}>دسته بندی هایی که در آن تخصص دارید را انتخاب نمایید</Text>
                        <View style={signup.catParent}>
                            {
                                !this.state.catLoader?
                                    this.state.categories.map((cat)=>{
                                        return(
                                            <View key={cat.id} style={{marginTop:15,borderBottomWidth:1,borderBottomColor:'#eee',padding:5}}>
                                                <Text style={signup.catTitle}>{cat.title}</Text>
                                                <View style={signup.subCatParent}>
                                                    {
                                                        cat.subs.map((sub)=>{
                                                            let index = this.state.checkBox.indexOf(this.state.checkBox.filter((cb)=>{if(cb.id == sub.id){return true}})[0])
                                                            return (
                                                                <View key={sub.id} style={{width:'50%',paddingRight:5,paddingLeft:5}}>
                                                                    <Item style={{borderBottomWidth:0}} onPress={()=>this.selectCat(index,sub.id)}>
                                                                        <Body style={{textAlign:'right',alignItems:'flex-end'}}>
                                                                            <Text style={signup.checkBoxText}>{sub.title}</Text>
                                                                        </Body>
                                                                        {
                                                                            this.state.checkBox.length?
                                                                                <CheckBox checked={this.state.checkBox[index].value}  onPress={()=>this.selectCat(index,sub.id)}/> :null
                                                                        }

                                                                    </Item>
                                                                </View>
                                                            )
                                                        })
                                                    }
                                                </View>
                                            </View>
                                        )
                                    }):
                                    <View style={createAd.loaderParent}><Spinner color={'#f2c200'} /></View>
                            }
                            <Item regular style={createAd.inputCode}>
                                <Textarea rowSpan={5} placeholder='توضیحات' style={createAd.textArea}  onChangeText={(value)=>this.setState({description:value})} value={this.state.description} />
                            </Item>
                            <Item regular style={this.state.requiredFields.includes(-1)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid'}]:signup.inputCode}>
                                <Input style={signup.inputCodeChild} placeholder='شماره ملی' keyboardType={'numeric'} onChangeText={(value)=>{this.setState({national_id:value})}} value={this.state.national_id}/>
                            </Item>
                            <Item picker  style={this.state.requiredFields.includes(-3)?{textAlign:'right',borderColor:'red',borderWidth:1,borderStyle:'solid'}:{textAlign:'right'}}>
                                <CustomPicker
                                    placeholder={'سابقه'}
                                    options={this.state.his}
                                    getLabel={item => item}
                                    containerStyle={signup.picker}
                                    value={this.state.history}
                                    onValueChange={value=>value!=null?this.setState({history:value}):this.setState({history:null})}
                                    fieldTemplateProps={{
                                        containerStyle: {
                                            borderBottomColor: '#fff',
                                            textAlign:'right'
                                        },
                                        textStyle: {
                                            color: '#454545',
                                            fontSize:18,
                                            textAlign:'right',
                                            right:0,
                                            position:'absolute',
                                            fontFamily:'IRANYekanMobileFN'
                                        },
                                    }}
                                    optionTemplateProps={{
                                        containerStyle: {
                                            backgroundColor: '#fff',
                                            height: 60,
                                            borderBottomWidth:0,
                                        },
                                        textStyle: {
                                            color: '#343434',
                                            right:15,
                                            fontFamily:'IRANYekanMobileFN',
                                            position:'absolute'
                                        }}}
                                />
                            </Item>
                            <Text style={[signup.description,{marginTop:10}]}>تاریخ تولد</Text>
                            <View style={{flexDirection:'row-reverse',justifyContent:'space-between',alignItems:'center'}}>
                                <Item regular style={this.state.requiredFields.includes(-1)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid',width:'20%'}]:[signup.inputCode,{width:'20%'}]}>
                                    <Input style={signup.inputCodeChild} placeholder='روز' keyboardType={'numeric'} onChangeText={(value)=>{this.setState({day:value})}} value={this.state.day}/>
                                </Item>
                                <Text>/</Text>
                                <Item regular style={this.state.requiredFields.includes(-1)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid',width:'20%'}]:[signup.inputCode,{width:'20%'}]}>
                                    <Input style={signup.inputCodeChild} placeholder='ماه' keyboardType={'numeric'} onChangeText={(value)=>{this.setState({month:value})}} value={this.state.month}/>
                                </Item>
                                <Text>/</Text>
                                <Item regular style={this.state.requiredFields.includes(-1)?[signup.inputCode,{borderColor:'red',borderWidth:1,borderStyle:'solid',width:'20%'}]:[signup.inputCode,{width:'20%'}]}>
                                    <Input style={signup.inputCodeChild} placeholder='سال' keyboardType={'numeric'} onChangeText={(value)=>{this.setState({year:value})}} value={this.state.year}/>
                                </Item>
                            </View>
                        </View>
                        <Button style={signup.loginButton} full onPress={this.submitForm.bind(this)} disabled={this.state.disableBtn}>
                            {
                                this.state.disableBtn?
                                    <Spinner color='#fff' />
                                    :
                                    <Text style={signup.loginButtonText}>ثبت</Text>
                            }
                        </Button>
                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={[single.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.pop()}>
                            <Icon style={single.footerIcon} name="ios-arrow-back" />
                            <Text style={single.footerText}>صفحه قبل</Text>
                        </Button>
                        <Button style={[shopL.pageFooterButton,{borderRightColor:'#eee',borderRightWidth:1}]} onPress={()=>Actions.myAds()}>
                            <Icon style={shopL.footerIcon} name="md-add" />
                            <Text style={shopL.footerText}>آگهی های من</Text>
                        </Button>
                        <Button style={shopL.pageFooterButton} onPress={()=>Actions.replace('list')}>
                            <Icon style={shopL.footerIcon} name="md-home" />
                            <Text style={shopL.footerText}>صفحه اصلی</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
    selectCat(index,id){
        if(this.state.checkBox[index].value){
            let catIndex = this.state.selectedCat.indexOf(id)
            if(catIndex>=0)
                this.state.selectedCat.splice(catIndex, 1)
        }else{
            this.state.selectedCat.push(id)
        }
        this.state.checkBox[index].value=!this.state.checkBox[index].value;
        this.forceUpdate();
    }
    submitForm(){
        this.setState({
            disableBtn:true
        })
        if(this.props.cats){
            if(this.state.selectedCat.length){
                pexios('/expert/update','post',{category_id:this.state.selectedCat,description:this.state.description,history:this.state.history,national_code:this.state.national_id,birthday:this.state.year+'-'+this.state.month+'-'+this.state.day}).then(res=>{
                    Actions.reset('myFabrika')
                    this.setState({
                        disableBtn:false
                    })
                }).catch(er=>{
                    this.setState({disableBtn:false})
                })
            }else{
                this.setState({disableBtn:false})
                Toast.show({
                    text: 'برای تغییر مشخصات باید حتما برای خود دسته بندی تایین کنید!',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'danger'
                })
            }

        }else{
            if(this.state.selectedCat.length){
                pexios('/expert/create','post',{category_id:this.state.selectedCat,description:this.state.description,history:this.state.history,national_code:this.state.national_id,birthday:this.state.year+'-'+this.state.month+'-'+this.state.day}).then(res=>{
                    Actions.reset('myFabrika')
                    this.setState({
                        disableBtn:false
                    })
                }).catch(er=>{

                    this.setState({disableBtn:false})
                })
            }else{
                this.setState({disableBtn:false})
                Toast.show({
                    text: 'برای تغییر مشخصات باید حتما برای خود دسته بندی تایین کنید!',
                    textStyle:{fontFamily:'IRANYekanMobileFN',textAlign:'right'},
                    duration: 3000,
                    type:'danger'
                })
            }
        }
    }
}