import React, {Component} from 'react'
import {Dimensions,I18nManager} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import {Router, Scene, Actions, Drawer, Lightbox} from 'react-native-router-flux'
EStyleSheet.build({
    $font :'IRANYekanMobileFN',
    $mainColor :'#f2c200'
})
//components

import splash from "./pages/splash";
import chatUser from "./pages/chatUser";
import chat from "./pages/chat";
import help from "./pages/help";
import Login from "./pages/login";
import LoginCheck from "./pages/loginCheck";
import Signup from "./pages/signup";
import editUser from "./pages/editUser";
import createShop from "./pages/createShop";
import expertRequest from "./pages/expertRequest";
import editShop from "./pages/editShop";
import List from "./pages/list";
import createAd from "./pages/createAd";
import editAd from "./pages/editAd";
import shopAdList from "./pages/shopAdList";
import shopList from "./pages/shopList";
import Single from "./pages/single";
import Expert from "./pages/expert";
import EditExpert from "./pages/eidtExpert";
import AdvanceSearch from "./pages/advanceSearch";
import myAds from "./pages/myAds";
import favAds from "./pages/favAds";
import myFabrika from "./pages/myFabrika";
import expertList from "./pages/expertList";
import tenderList from "./pages/tenderList";
import tenderSingle from "./pages/tenderSingle";
import requestedExperts from "./pages/requestedExperts";
import creditRequest from "./pages/credit-request";
import {Icon, Root, Text, View} from "native-base";
import {TouchableOpacity} from "react-native";
export default class App extends Component{
    componentWillMount(){
        I18nManager.allowRTL(false)
    }
    componentDidMount(){
        I18nManager.allowRTL(false)
    }
    componentWillUnmount(){
        I18nManager.allowRTL(false)
    }
    render(){
        return(
            <Root>
                <Router>
                    <Scene>
                        <Scene key="splash" component={splash} hideNavBar initial />
                        <Scene key="chatUser" component={chatUser} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>پیام شخصی</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="chat" component={chat} hideNavBar />
                        <Scene key="tenderList" component={tenderList} hideNavBar />
                        <Scene key="help" component={help} hideNavBar />
                        <Scene key="list" component={List} hideNavBar />
                        <Scene key="expertList" component={expertList} hideNavBar />
                        <Scene key="myAds" component={myAds} hideNavBar />
                        <Scene key="favAds" component={favAds} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>آگهی های مورد علاقه</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="createShop" component={createShop} hideNavBar />
                        <Scene key="editShop" component={editShop} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>ویرایش فروشگاه</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="loginCheck" component={LoginCheck} hideNavBar />
                        <Scene key="login" component={Login} hideNavBar />
                        <Scene key="expertRequest" component={expertRequest} hideNavBar />
                        <Scene key="requestedExperts" component={requestedExperts} hideNavBar />
                        <Scene key="signup" component={Signup} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.reset('loginCheck')}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>تکمیل عضویت</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="editUser" component={editUser} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>ویرایش مشخصات</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="createAd" component={createAd} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>ایجاد آگهی</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="editAd" component={editAd} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>ویرایش آگهی</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="advanceSearch" component={AdvanceSearch} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>جستجوی پیشرفته</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="shopAdList" component={shopAdList} hideNavBar />
                        <Scene key="shopList" component={shopList} hideNavBar />
                        <Scene key="single" component={Single} hideNavBar />
                        <Scene key="tenderSingle" component={tenderSingle} hideNavBar />
                        <Scene key="myFabrika" component={myFabrika} hideNavBar />
                        <Scene key="expert"  component={Expert} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>مشاهده کارشناس</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="editExpert"  component={EditExpert} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>ویرایش کارشناس</Text>} navigationBarStyle={{elevation:0}} />
                        <Scene key="creditRequest"  component={creditRequest} renderLeftButton={<TouchableOpacity style={{marginLeft:15}} onPress={()=>Actions.pop()}><Icon name={'ios-arrow-back'} /></TouchableOpacity>} renderRightButton={<View></View>} renderTitle={<Text style={{flex:1,textAlign:'center',fontFamily:'IRANYekanMobileFN'}}>درخواست اعتبار</Text>} navigationBarStyle={{elevation:0}} />
                    </Scene>
                </Router>
            </Root>
        )
    }
}