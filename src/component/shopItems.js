import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity} from "react-native";
import {Icon, Text, View,Item} from "native-base";
import {url} from "./helper";
import {Fabrika} from "../assets/styles";
export default class Shop extends Component{
    constructor(props){
        super(props)
        this.state={
            showDetails:false
        }
    }
    render(){
        const { shop } = this.props;
        return(
            <View>
                <Item>
                    <TouchableOpacity style={[Fabrika.buttons,{justifyContent:'space-between'}]} onPress={()=>this.setState({showDetails:!this.state.showDetails})}>
                        <View style={{flexDirection:'row-reverse'}}>
                            <Icon style={Fabrika.icons} type="FontAwesome" name={'shopping-bag'}/>
                            <Text style={Fabrika.text}>{shop.title}</Text>
                        </View>
                        <Icon type="FontAwesome" name={'chevron-down'}/>
                    </TouchableOpacity>
                </Item>
                {
                    this.state.showDetails?
                        <View>
                            <Item>
                                <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.editShop({sh:shop})}>
                                    <Text style={[Fabrika.text,{marginRight:20}]}> - ویرایش مشخصات  {shop.title}</Text>
                                </TouchableOpacity>
                            </Item>
                            <Item>
                                <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.createAd({shopId:shop.id})}>
                                    <Text style={[Fabrika.text,{marginRight:20}]}> - ثبت آگهی برای  {shop.title}</Text>
                                </TouchableOpacity>
                            </Item>
                            <Item>
                                <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.shopAdList({id:shop.id})}>
                                    <Text style={[Fabrika.text,{marginRight:20}]}> - آگهی های  {shop.title}</Text>
                                </TouchableOpacity>
                            </Item>
                            {
                                shop.type=='normal'?
                                <Item>
                                    <TouchableOpacity style={Fabrika.buttons} onPress={()=>Actions.creditRequest({id:shop.id})}>
                                        <Text style={[Fabrika.text,{marginRight:20}]}>- درخواست اعتبار  {shop.title}</Text>
                                    </TouchableOpacity>
                                </Item>:null
                            }

                        </View> :null
                }

            </View>
        )
    }
}