import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import { list } from './../assets/styles';
import {Image, TouchableOpacity} from "react-native";
import {Icon, Text, View ,Badge} from "native-base";
import {url,priceFormat} from "./helper";
import moment from 'moment-jalaali'
export default class ListComponent extends Component{
    constructor(props){
        super(props)
        this.state={
            hasImg:false
        }
    }
    render(){
        const { ad,myAd,rel } = this.props;
        return(
            <TouchableOpacity style={[list.listParent]} onPress={()=>Actions.single({ad:ad,myAd:myAd})} activeOpacity={.8}>
                <View style={list.imageParent}>
                    {
                        ad.attachmentsImage.length?<Image source={{uri :url+'/files?uid='+ad.attachmentsImage[0].uid+'&width=300&height=300'}}  style={{width:'100%',height:140}} />:<Image source={require('../assets/images/no-image.png')} style={{width:'100%',height:140}} />
                    }
                </View>
                <View style={list.detailParent}>
                    <Text style={list.detailTitle}>{ad.title}</Text>
                    <View style={{marginTop:10}}>
                        <View style={[list.detail,{flexWrap:'wrap',justifyContent:'flex-start'}]}>
                            {
                                ad.category.items.filter((imp)=>{return imp.important==1}).map((itm,index)=>{
                                    return  <Text key={itm.id} style={list.importantElement}>{index==0?'':' ، '}{itm.title}:{JSON.parse(ad.data)?JSON.parse(ad.data)[itm.id]?itm.type=='switch'?JSON.parse(ad.data)[itm.id]==1?'بله':'خیر':JSON.parse(ad.data)[itm.id]:'':''}</Text>
                                })
                            }
                        </View>
                        {
                            ad.shop?
                                ad.shop.type=='silver'?
                                    <View style={list.detail}>
                                        <Text style={[list.detailText,{textAlign:'right'}]}>اعتبار</Text>
                                        <Badge style={{backgroundColor:'#a4b0be'}}><Text style={list.shopStarText}>اعتبار نقره ای</Text></Badge>
                                    </View>:
                                    ad.shop.type=='gold'?
                                        <View style={list.detail}>
                                            <Text style={[list.detailText,{textAlign:'right'}]}>اعتبار</Text>
                                            <Badge style={{backgroundColor:'#eccc68'}}><Text style={list.shopStarText}>اعتبار طلایی</Text></Badge>
                                        </View>
                                :null:null
                        }

                        <View style={list.detail}>
                            {
                                ad.priority=='immediate'?
                                    <View style={{flexDirection:'row-reverse',justifyContent:'space-between',flex:1}}>
                                        <Text style={[list.detailText,{textAlign:'right'}]}>وضعیت:</Text>
                                        <View>
                                            <Text style={list.badgeText}>فوری</Text>
                                        </View>
                                    </View>
                                    :ad.priority=='special'?
                                    <View style={{flexDirection:'row-reverse',justifyContent:'space-between',flex:1}}>
                                        <Text style={[list.detailText,{textAlign:'right'}]}>وضعیت:</Text>
                                        <View>
                                            <Text style={list.badgeText}>ویژه</Text>
                                        </View>
                                    </View>
                                    :ad.priority=='immediate-special'?
                                     <View style={{flexDirection:'row-reverse',justifyContent:'space-between',flex:1}}>
                                         <Text style={[list.detailText,{textAlign:'right'}]}>وضعیت:</Text>
                                         <View>
                                             <Text style={list.badgeText}>ویژه و فوری</Text>
                                         </View>
                                     </View>:null

                            }
                        </View>
                        <View style={list.detail}>
                            <Text style={[list.detailText,{textAlign:'right'}]}>قیمت:</Text>
                            <Text style={list.detailText}>{ad.price?priceFormat(ad.price)+' تومان':'تماس بگیرید'}</Text>
                        </View>
                        <View style={list.detail}>
                            <Text style={[list.detailText,{textAlign:'right'}]}>تاریخ آگهی:</Text>
                            <Text style={list.detailText}>{moment(ad.created_at).format('jYYYY/jM/jD')}</Text>
                        </View>
                        <View style={{paddingTop:5}}></View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}