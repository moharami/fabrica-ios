import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import { list } from './../assets/styles';
import {Image, TouchableOpacity} from "react-native";
import {Icon, Text, View ,Badge} from "native-base";
import {url,priceFormat} from "./helper";
import moment from 'moment-jalaali'
export default class ListComponent extends Component{
    constructor(props){
        super(props)
        this.state={
            hasImg:false
        }
    }
    render(){
        const { ad } = this.props;
        return(
            <TouchableOpacity style={[list.listParent]} onPress={()=>Actions.tenderSingle({ad:ad})} activeOpacity={.8}>
                <View style={list.imageParent}>
                    {
                        ad.attachmentsImage.length?<Image source={{uri :url+'/files?uid='+ad.attachmentsImage[0].uid+'&width=300&height=300'}}  style={{width:'100%',height:140}} />:<Image source={require('../assets/images/no-image.png')} style={{width:'100%',height:140}} />
                    }
                </View>
                <View style={list.detailParent}>
                    <Text style={list.detailTitle}>{ad.title}</Text>
                    <Text style={[list.detailTitle,{fontSize:14}]}>{ad.description}</Text>
                    <View style={{marginTop:10}}>
                        <View style={list.detail}>
                            <Text style={[list.detailText,{textAlign:'right'}]}>تاریخ شروع:</Text>
                            <Text style={list.detailText}>{moment(ad.created_at).format('jYYYY/jM/jD')}</Text>
                        </View>
                        <View style={list.detail}>
                            <Text style={[list.detailText,{textAlign:'right'}]}>تاریخ پایان:</Text>
                            <Text style={list.detailText}>{moment(ad.time).format('jYYYY/jM/jD')}</Text>
                        </View>
                        <View style={{paddingTop:5}}></View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}