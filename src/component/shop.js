import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import {Image, ImageBackground, TouchableOpacity} from "react-native";
import {Icon, Text, View} from "native-base";
import {url} from "./helper";
import moment from 'moment-jalaali'
import {shopL} from "../assets/styles";
export default class Shop extends Component{
    constructor(props){
        super(props)
        this.state={
            hasImg:false
        }
    }
    render(){
        const { sh } = this.props;
        return(
            <TouchableOpacity style={{marginTop:10}} onPress={()=>Actions.shopAdList({id:sh.id,isOwner:sh.is_owner})}  activeOpacity={.8}>
                {/*<ImageBackground source={{ uri : sh.attachmentsImage.filter((img)=>{if(img.filename=='cover')return true}).length?url+'/files?uid='+sh.attachmentsImage.filter((img)=>{if(img.filename=='cover')return true})[0].uid+'&width=400&height=200':url+'/images/shopBg.png'}} style={shopL.shopBackground}>*/}
                <View style={{backgroundColor:'white'}}>
                    <View style={{padding:20}}>
                        <View style={{flexDirection:'row-reverse',justifyContent:'space-between'}}>
                            {
                                sh.address==''?
                                    <Image style={{height:64,width:64,borderRadius:32}} source={{uri:sh.user.attachments? url+'/files?uid='+sh.user.attachments.uid+'&width=64&height=64':url+'/images/profile.png'}} />:
                                    <Image style={{height:64,width:64,borderRadius:32}} source={{uri:sh.attachmentsImage.filter((img)=>{if(img.filename=='logo')return true}).length? url+'/files?uid='+sh.attachmentsImage.filter((img)=>{if(img.filename=='logo')return true})[0].uid+'&width=64&height=64':url+'/images/profile.png'}} />
                            }
                            <View>
                                <View style={{flexDirection:'row-reverse',justifyContent:'flex-start'}}>
                                    <Text style={shopL.starText}>امتیاز:</Text>
                                    {
                                        sh.type=='gold'?
                                            <Icon style={[shopL.star,{color:'#eccc68'}]} name={'md-star'}/>:
                                            sh.type=='silver'? <Icon style={[shopL.star,{color:'#a4b0be'}]} name={'md-star'}/>:<Icon style={[shopL.star,{color:'#454545'}]} name={'md-star-outline'}/>
                                    }
                                </View>
                            </View>
                        </View>
                        <View style={{marginTop:20}}>
                            <Text style={[shopL.shopTitle,shopL.bold]}>{sh.title}</Text>
                            <Text style={shopL.shopText}>آدرس: {sh.address}</Text>
                            <Text style={shopL.shopText}>
                                تلفن تماس:  {sh.tell}
                            </Text>
                        </View>
                    </View>
                </View>
                {/*</ImageBackground>*/}
            </TouchableOpacity>
        )
    }
}